# Tenth week
**4 hours**

- Looked over the base application created by Karel and Simo and looked into how to tie the front and back end together
- Helped with the Miro board
- Did SM stuff like reading up on project rules, making reminders, making sure tasks looked good

# Eleventh week
**10 hours, 40 minutes**

Reviewed seed transaction mockup, p2p transaction view mockup and new transaction diagram.

Reviewed Karel's H2 database. Also reviewed Richard's modify transaction mockup.

Spent a while looking over Sergei's pull request, which was incorrectly formatted in all the ways. Explained to him the correct way to format things. Then merged his pull request with mine (BAN-51), since he redid my work. 

Then I created a register component for front end and messed around with git a lot to get the branch pushed, since I'm new to using git on IntelliJ.

Also reminded everyone about their tasks and logging, although I could not access everyone's Tempo to review their logging. So I couldn't make sure whether they had actually all worked and how much.

Tried to create a view for adding both seed and common transaction, but for some reason the form did not dynamically change according to transaction type in Angular. So after a bit of trying I gave up and just made two separate components for them.

Discussed some tasks with Liisa and Rain. With Rain we also talked about adapting a postgres database despite Karel adding the H2, because we might need to deploy and then a H2 database will not be enough. Better to add postgres now than later when everything is built.

Also reminded everyone to review and system test pull requests since only a few people had done that so far.

Reviewed Sander's P2P view and added a comment. Also told him how our PR system works: you make a PR -> someone reviews it and approves -> system test is carried out -> the reviewer merges to master if everything is ok.

Simo also left a comment on my register PR and I made changes according to it. It was a very good point that registering an account is only for admins and I had forgotten about it and automatically added register to the main login page as per usual. Also added success notifications to all my views.

# Twelfth week
**10 hours, 30 minutes**

Reviewed Sander's bug fix and added feedback popups to tasks BAN-27, BAN-36, BAN-47.

Talked to some people about their less than 8 hours of work but agreed that since the week was so horrible for all of us, I'd let people do more work this week to make up.

Connected all the admin views and spent a suprising amount of time on CSS, so it would look okay on both desktop and most mobile views.

Helped Karel with his front-end authentication and turned promises into observables.

# Thirteenth week
**9 hours**

Made some services and examples of using them so that other people can make some as well. Added last of P2P front-end logic.

It had been a while since I had done anything practical with Angular and relearning the customs and ways to do things took a bit of time. Especially so for all the datatypes being returned when calling get and post etc, how to make that returned data usable took a while of googling and looking at past projects. It's easy to do randomly, but if efficiency is in question, I needed to look at past examples. 

Reviewed Sander's seed transaction logic.

# Fourteenth week
**9 hours**

Added better error-handling to p2p. 

I had major problems with nested requests to API, because in Angular it is good practice to use concatmap, mergemap, switchmap etc instead of just nested observables. This however made error handling in the outer request weird, it didn't act like it was supposed to. So I spent too many hours on looking for what was wrong with my code and ended up refactoring it to not use nested calls (turned out it was just a matter of misplaced parentheses).
Reviewed all weekly tasks but my own BAN-88.

Also co-operated in creating the video.

