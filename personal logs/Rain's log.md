# 14.11.2020
**4 hours**

- Researched Node-RED and watched tutorials.
- Tried out reading data, sending data, designing views for our banking application, designing the flow.
- Logging the work and writing a summary.

# Main work - 18.25h

- I did not take into account the time I had to learn all the Java REST API techniques.
- I mainly worked on the back-end and I have logged 18.25 hours of work on that.
![IMG](https://i.ibb.co/mNtFk4K/Week1.png)
![IMG2](https://i.ibb.co/cTM6rRC/Week2.png)

# Smaller work - ~16h

- Meetings, small bug fixes and all work that took time less than 20 minutes. 
- Integrating some views with back-end.

# Reflection
I did not have much experience with Java and IntellIJ, H2 database - so it was really cool to learn this by taking a lead role in doing this. I enjoyed this very much and I think it will be a useful experience in my further career. I did not do much work on the diagrams as it was not my role and I wanted to learn something new. It was a great experience. The diagrams helped me design the back-end greatly.