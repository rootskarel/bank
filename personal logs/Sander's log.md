# Sander's log
## Week 10
* Implemented the login component in angular
* Made an activity diagram of creating a new transaction
## Week 11
* Made an activity diagram of creating a seed transaction
* Implemented the P2P transaction view and modals
## Week 12
* Implemented the seed transaction REST endpoint
## Week 13
* Connected new transaction angular component to the back end
* Connected seed transaction angular component to the back end
* Connected modify transaction angular component to the back end

## Week 14
* Did the first part of the demo in the final video