# Week 10
* Role assigning, first tasks.

# Week 11     
* Worked on the User and Administrator personas.     
* Had a team meeting.       
 
# Week 12    
* Made some changes to the personas and created a third one.     
* Reviewd the front-end view connection task.     
* Created a user login diagram.    
* Spent some time on getting the whole thing running.      
* Did some further testing on the whole system.     
* Had a team meeting.    
 
# Week 13     
* Created a component diagram.    
* Read through the architecture report tasks. 

# Week 14
* Final testing.
* Presentation movie. 