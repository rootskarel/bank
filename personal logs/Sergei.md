# Sergei's log
## Week 10
* We planned the whole project with the team.
* Technologies, roles, deadlines, diagramms, architecture, mockup, design, repository and development environment, etc.
* One team meeting
## Week 11
* Transaction info, edit and revoke diagram
* Transaction history view (front-end)
* Revoking a transaction view (front-end)
* Two team meetings
## Week 12
* Modifying a transaction view (front-end)
* Transaction info, edit and revoke diagram (i got a feedback and I had to redo the diagram, as I misunderstood the functionality)
* Two team meetings
## Week 13
* Transaction info, edit and revoke diagram (got feedback again and again had to redo the diagram a bit)
* Connect transactions history view to back-end (front-end + back-end)
* Bugs from week 11, 12, 13
* Two team meetings
* Video preparation: roles, text, speach recording and etc