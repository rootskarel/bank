# Architecture Report of the Bank application

This is the detailed documentation in regard to the architecture and 
modeling approaches used while developing the banking application for the Systems Modeling course.

The developed banking application front-end is based on the Angular JavaScript framework version 11 and our back-end on
 Java 11 and the Spring Boot framework version 2.3.5.

## Architectural artefacts used in the project

Below you can find all architectural artefacts developed during this project.
Some artefacts have both old and latest versions to highlight the evolution of our architecture.

### Event Storming

[Miro Board](https://miro.com/app/board/o9J_kgKrdjY=/)

### Personas

[Persona Anna](diagrams/personas/User-personas-anna.png)

[Persona Eric (old)](diagrams/personas/old/User-personas-eric-old.png)

[Persona Eric (latest)](diagrams/personas/User-personas-eric.png)

[Persona Louisa (old)](diagrams/personas/old/User-personas-louisa-old.png)

[Persona Louisa (latest)](diagrams/personas/User-personas-louisa.png)

### Activity Diagrams

[New Seed Transaction](diagrams/activity-diagrams/admin_new_seed_transaction_activity_diagram.png)

[New Transaction (old)](diagrams/activity-diagrams/old/user_new_transfer_activity_diagram.png)

[New Transaction (latest)](diagrams/activity-diagrams/admin_new_transfer_activity_diagram.png)

### Sequence Diagrams

[User Login (old)](diagrams/sequence-diagrams/old/user_login_sequence_diagram_old1.png)

[User Login (latest)](diagrams/sequence-diagrams/user_login_sequence_diagram.png)

[Account Creation (old)](diagrams/sequence-diagrams/old/bank_account_creation_seq_1.png)

[Account Creation (latest)](diagrams/sequence-diagrams/bank_account_creation_seq_2.png)

[Transaction Edit (old1)](diagrams/sequence-diagrams/old/transaction_edit_seq_diagram_old1.png)

[Transaction Edit (old2)](diagrams/sequence-diagrams/old/transaction_edit_seq_diagram_old2.png)

[Transaction Edit (old3)](diagrams/sequence-diagrams/old/transaction_edit_seq_diagram_old2.png)

[Transaction Edit (latest)](diagrams/sequence-diagrams/transaction_edit_seq_diagram.png)

[Transaction History (old1)](diagrams/sequence-diagrams/old/transaction_history_seq_diagram_old1.png)

[Transaction History (latest)](diagrams/sequence-diagrams/transaction_history_seq_diagram.png)

### Class Diagrams

[User & Transaction Data Model (old)](diagrams/class-diagrams/old/user_transaction_data_model_old.png)

[User, Account, Code & Transaction Data Model (Fulib)](diagrams/class-diagrams/fulib_class_diagram.svg)

[User, Account, Code & Transaction Data Model (Latest)](diagrams/class-diagrams/class_diagram_new.png)

### Object Diagrams

[User, Account, Code & Transaction Data Model 1 (old)](diagrams/object-diagrams/old/object_diagram_1.png)

[User, Account, Code & Transaction Data Model 1 (Latest)](diagrams/object-diagrams/object_diagram_new1.png)

[User, Account, Code & Transaction Data Model 2 (old)](diagrams/object-diagrams/old/object_diagram_2.png)

[User, Account, Code & Transaction Data Model 2 (Latest](diagrams/object-diagrams/object_diagram_new2.png)

### Component Diagrams

[Application Component Diagram](diagrams/component-diagrams/component_diagram.png)


### Mockups

[Login View Desktop](mockups/login_view_desktop.png)

[Login View Mobile](mockups/login_view_mobile.png)

[P2P Transaction View Desktop](mockups/user/p2p_transaction_desktop.png)

[P2P Transaction View Mobile](mockups/user/p2p_transaction_mobile.png)

[Account Creation View Desktop](mockups/admin/account_creation.png)

[Seed Transaction View Desktop](mockups/admin/seed_transaction.png)

[New Transaction View Desktop](mockups/admin/new_transaction.png)

[Transaction History View Desktop](mockups/admin/transaction_history.png)

[Transaction Details View Desktop](mockups/admin/transaction_details.png)

[Transaction Revoke View Desktop](mockups/admin/revoking_transaction.png)

[Transaction Edit View Desktop](mockups/admin/modifying_transaction.png)

## Design patterns used in the project

In this section we describe the different design patterns employed in the development of our Banking application.

### The Model-View-Controller (MVC) pattern

The MVC pattern is an essential design pattern in modern web development. It is commonly used for developing user
 interfaces and helps to separate the data (model), the business logic (controller) and the graphical view.
 
 In our project this pattern makes sense because it keeps the code much more maintainable when the three main
  components that make up the graphical user interface are separated. It also enabled much faster development because
   the view and the controller can be developed by different developers in parallel. It also makes it easy to have
    the same business logic control different dynamic views and have many views for one model thus reducing code
     duplication.
     
Examples in our code:

#### Models

1.) [Account](front-end/src/app/model/account.ts)

2.) [Code](front-end/src/app/model/code.ts)

3.) [Transaction](front-end/src/app/model/transaction.ts)

4.) [User](front-end/src/app/model/user.ts)

#### Views

1.) [Login](front-end/src/app/component/login/login.component.html)

2.) [P2P Transaction](front-end/src/app/component/p2p-transaction/p2p-transaction.component.html)

3.) [Account Creation](front-end/src/app/component/register/register.component.html)

#### Controllers

1.) [Login](front-end/src/app/component/login/login.component.ts)

2.) [P2P Transaction](front-end/src/app/component/p2p-transaction/p2p-transaction.component.ts)

3.) [Account Creation](front-end/src/app/component/register/register.component.ts)

### The Repository pattern

Repository classes are used to encapsulate the logic required to access data sources by centralizing common data
 access functionality. Furthermore, the repository pattern is a sub-pattern for Domain-Driven-Design and helps to
  keep business domain close to the code by giving each data model their own repository for data access.
 
 In our project this pattern makes sense because the application requires a lot of different operations with the data
  in the database and this pattern provides better maintainability of data access logic. The Spring Boot framework
   used in our application enables easy initialization of repositories with minimal setup and saved a lot of time in setting
    up the data access logic.
    
Examples in code:

1.) [UserRepository](src/main/java/ee/ut/banking/repository/UserRepository.java)

2.) [AccountRepository](src/main/java/ee/ut/banking/repository/AccountRepository.java)

3.) [CodeRepository](src/main/java/ee/ut/banking/repository/CodeRepository.java)
    

### The Builder pattern

The builder design pattern provides a flexible solution to object creation. It separates the construction of complex
 objects from its representation and simplifies creating new objects without having to produce all possible
  constructors for given object. It makes sense in our project because in our back-end logic many objects are created
   and this pattern helps to write clean and maintainable code.

The builder pattern is used for most of our data models and data transfer objects. We have implemented the builder
 pattern using the Lombok plugin which generates builders for classes when the @Builder annotation is added to the
  class definition.

Examples in code:

1.) [Account class](src/main/java/ee/ut/banking/model/Account.java)

2.) [Transaction class](src/main/java/ee/ut/banking/model/Transaction.java)

3.) [Transaction class](src/main/java/ee/ut/banking/model/Transaction.java)

4.) [TransactionRequest class](src/main/java/ee/ut/banking/controller/domain/TransactionRequest.java)

### The Representation State Transfer (REST) pattern

Our application back-end is entirely built on the REST pattern. This pattern has become in many ways a standard for
 web applications when communicating with back-end APIs. 
 
 Using the REST pattern makes sense in our application
  because we wanted to have a standardized API to access and manipulate resources in our banking application and to
   separate the back-end and front-end of our application in order to have a client-server relationship. REST APIs
    are also stateless by design and that makes scalability for our application easy to achieve.
  
Examples in code:

1.) [TransactionController](src/main/java/ee/ut/banking/controller/TransactionController.java)

2.) [UserController](src/main/java/ee/ut/banking/controller/UserController.java)

3.) [CodeController](src/main/java/ee/ut/banking/controller/CodeController.java)


### The Observer pattern

The observer pattern is one of the most used patterns in modern front-end development. In our application we use the
 Angular JavaScript framework which is highly dependent on using the observer pattern for exchanging events between
  different front-end components and objects. Observable objects are used in Angular for both http communication with
   the back-end services and Angular's inner event handling system. 
   
   In our application this pattern makes sense because we are building a reactive web application 
   that must change depending on different user inputs and button clicks. As such this is best achieved by using
    observable objects and event listeners. For data fetching this also means that the observable requests can be
     cancelled or "observed" more than once as opposed to JavaScript Promises that cannot be cancelled or consumed
      more than once.
     
Examples in code:

1.) [http-service](front-end/src/app/service/http.service.ts)

2.) [auth-interceptor](front-end/src/app/interceptor/auth.interceptor.ts)     

## Significant architectural changes in time

We started off with naive user and transaction models as illustrated here 
[here](diagrams/class-diagrams/old/user_transaction_data_model_old.png). 

Through several iterations of redesigning and we ended up with the data models seen 
on [this diagram](diagrams/class-diagrams/class_diagram_new.png)


## Usage manual of the project

In the following section we describe how to setup and run the application.

### Project setup

Requirements: Java 11 and latest Node.js installed from https://nodejs.org/en/

It is easiest to run the project using IntelliJ, as such we presume that the following steps are performed in IntelliJ.

1.) Import the project in IntelliJ

2.) Import the back-end folder src/ in IntelliJ using the "File" -> "New" -> "Module from existing sources"

3.) To make sure that the project was imported correctly and all dependencies were installed refresh gradle do the
 following:

![gradle refresh](images/guide/gradle_refresh.png)

![dependencies refresh](images/guide/refresh_dependencies.png)

4.) If you do not have lombok plugin installed in IntelliJ you should install it under "File" -> "Settings" -> "Plugins"

5.) You should also enable the lombok plugin under "File" -> "Settings" -> Search for "lombok" and make sure you have
 the following settings:
 
 ![dependencies refresh](images/guide/lombok_settings.png)
 
6.) Make sure you have annotations processor enabled in IntelliJ under "File" -> "Settings" -> Search for "annotation
" and check that you have it enabled:

 ![dependencies refresh](images/guide/annotations_settings.png)


### Running the project

To run the project you must run the back-end and front-end project separately. 

#### Front-end

1.) To run the front-end project you must first install the dependencies by opening up a terminal, starting from the
 project root folder and going to the front-end folder with the command: "cd front-end"
 
2.) Next you must install the dependencies with the command: "npm install"

3.) Finally you must run the front-end application with command: "npm run start"

### Back-end

To run the back-end project you can simply run the main class that is located 
[here](src/main/java/ee/ut/banking/BankingApplication.java)


### Accessing the application

Once the back-end and front-end projects are successfully running you can access the application in your browser at
 http://localhost:4200/

To login as a customer you can use the username "test" and password "test" or username "test3" and password "test"

To login as an administrator you can use the username "test2" and password "test"