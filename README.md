# Bank application for Systems Modelling

## Team Log

## 10.11

We had our first big official project meet-together, all of us were present except for Sergei. The previous day we had agreed upon the inital team composition of:

Karel - Product Owner & Back-end Developer

Janna-Liina - Scrum Master & Developer (back/front) assistance

Richard - Architecture Owner & Developer (back/front) assistance

Simo - SM/PO assistance & Developer

Liisa - QA/Developer & SM assistance

Rain - Developer (back/front) & AO/PO assistance

Sander - Developer (Front-end) & AO assistance

Karmen - AO/SM assistance & Developer

Sergei - Developer

We agreed to write the project using Angular and Spring. We decided on a Kanban approach instead of Scrum. 

We spent the lab hours (and a little longer) on writing down our first user stories (that work can be found here https://docs.google.com/document/d/1JB6ePnbcRoqxiPsZ3hJ13jJwL16bc9h9RI0NE-UCdyY/edit) as well as editing our Miro board. Our Miro board can be found here: https://miro.com/app/board/o9J_kgKrdjY=/

Richard took most of the Miro editing on himself (also made one mockup found here https://discordapp.com/channels/747472464304668727/771744830690295808/776162943981060117). The rest of us discussed how to mark down our stories and how to create them out of the Miro board. Since we used Gherkin notation in another course we agreed on a Gherkin imiation notation for the acceptance criteria. Also, Karel proposed the overall structure of our stories which consist of an ID, title, actor, content, acceptance criteria and estimate (very basic one for now, later will prioritise better when stories are complete and added to Jira).

Karel set up our Jira and we started adding stories there. 

### 14.11 Node-RED Exploration

After testing out Node-RED, we have decided not to use it in our project. It seems like an easy drag-and-drop method of designing front-end, but does require some work to get used to it.

Reading data and making requests is understandable enough, but the first instinct on how to read data from input fields or pass data to some elements does not always seem elementary. We tried designing some basic views for our banking app, but this already required tutorial exploration and rewatching the lecture. Since the banking app will have strict deadlines, we cannot have all our front-end developers spending time on learning new UI tools. This is only because we assess that this tool is not easy enough to learn from zero experience. It would be time-wasting only in the sense that "this course and the project is already consuming enough so why make it more challenging".

We used it for creating some parts of the user interface and experimented with reading inputs and controllers. User interface design was not as easy as expected and moving between views could prove time-consuming to implement. Some simple flows did not always work which was frustrating. If we were to use Node-RED we would be in danger of being stuck on some simple problems just because we lack experience with this kind of tool. 
![node-red](images/node-red-bank-test.PNG)

Node-RED has potential. Creating views, flows, reading data and communication with controllers was not overly complicated. But it would require a couple of weeks to learn how to use it more deeply (that would include focusing more on it in lectures and practice sessions) in order to understand it more deeply. We would use it for a more simple homework, but for our big project - No.

## Eleventh week

During the lab on tuesday everyone was present and active. PO, SM and AO were called away for a bit. We talked about the user and transaction class diagram made by Richard and agreed on what he had done. We then divided tasks among us and made sure everyone had something to do this week. 

We agreed on logging our time in the manner that we wish, either on our personal portfolio or in tempo. 

We had another meeting on Sunday morning for an hour. Everyone was present except Sergei. We went over the progress so far, looked at tasks in development and went over some mockups and data models. 

There was a lengthy discussion about the P2P transfers and code generation, where we agreed on a pretty simplistic approach of one code per person for asking money, which remains active until another is started. Code will be a separate entitiy. 

We also briefly touched upon how to better trace changes to transactions, but then decided the current diagram by Richard is the approach we will take. 

Before the meeting ended, we gave some tasks to people to make sure everyone has something to do.

## Twelfth week

During the tuesday lab we went over the tasks in our kanban board. We discussed what we had done, what needs to be done and how much time we have to do everything. We split unassigned tasks among each other and talked about if we should have a H2 or postgres database, but left it undecided until we know whether the app needs to be deployed or not. Afterwards we stuck to using H2 since no actual deployment was required and H2 does not need a local database to be created and configured, thus it is much easier to quickly be run and used by anyone.

On Sunday we had a meeting. We talked about the progress so far, we had done a lot during the week. There was a lengthy discussion about transaction statuses and how we would deal with traceability. Richard wanted to achieve revoking without adding any extra attributes to the transaction model. 

We went over unsolved issues and made sure everyone is on track. Told everyone to log time, since most of us didn't have our weekly 8 hours filled.

## Thirteenth week

We had a short lab this week where we went over our tasks, which we didn't have many of any more. Most of them were in front end. Karel showed how to get the application running.

Also, our architecture report was made:

### [Architecture Report](ArchitectureReport.md)

## Fourteenth week

Karmen and Janna showed portfolios and got feedback on them. What to take away - add links everywhere! Meanwhile everyone else split the remaining tasks, made a component diagram of our system and started planning for the final presentation video. 

Here's our Google Docs planning: https://docs.google.com/document/d/1flNyZ94ZcQv3Rjp5jimPF_XCHVCLmwLBI29gvkSraUk/edit and also here's our slides:https://docs.google.com/presentation/d/1fpz7yOkKJXpJ8Y3wkmwSMEsPfCjXUDe0iW8O3IjOKgg/edit#slide=id.p2

The rest of the week we completed the project and worked on the final presentation video.

[The Final Presentation Video](https://youtu.be/kGAbrQs785Y)



