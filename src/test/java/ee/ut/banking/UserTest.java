package ee.ut.banking;

import ee.ut.banking.exception.ConstraintException;
import ee.ut.banking.model.Account;
import ee.ut.banking.model.User;
import ee.ut.banking.repository.AccountRepository;
import ee.ut.banking.repository.UserRepository;
import ee.ut.banking.service.AccountService;
import ee.ut.banking.service.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.passay.PasswordGenerator;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.Set;

@ExtendWith(MockitoExtension.class)
public class UserTest {

    private static final String USER1_USERNAME = "Harry93";
    private static final String USER1_NAME = "Harry";
    private static final String USER1_PASSWORD = "test";
    private static final User user1 = User.builder()
                                          .username(USER1_USERNAME)
                                          .name(USER1_NAME)
                                          .password(USER1_PASSWORD)
                                          .id(1)
                                          .build();
    private static final User user2 = User.builder()
                                          .username("Sally88")
                                          .password("user")
                                          .id(2)
                                          .build();
    private static final String ACCOUNT1_NAME = "ABC123";
    private static final Account acc1 = Account.builder()
                                               .name(ACCOUNT1_NAME)
                                               .balance(100)
                                               .users(Set.of(user1))
                                               .build();
    private static final String ACCOUNT2_NAME = "321CDF";
    private static final Account acc2 = Account.builder()
                                               .name(ACCOUNT2_NAME)
                                               .balance(100)
                                               .users(Set.of(user2))
                                               .build();

    @Mock
    private UserRepository userRepository;
    @Mock
    private AccountRepository accountRepository;
    @Mock
    private BCryptPasswordEncoder passwordEncoder;
    @Mock
    private PasswordGenerator passwordGenerator;

    @InjectMocks
    private UserService userService;
    @InjectMocks
    private AccountService accountService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }


    @Test
    public void givenValidDetails_whenCreatingNewAccount_thenCreatedAccountReturned() throws ConstraintException {
        // TODO: add tests
    }
}