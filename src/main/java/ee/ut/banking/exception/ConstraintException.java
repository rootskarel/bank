package ee.ut.banking.exception;

public class ConstraintException extends Exception {

    public ConstraintException(String message) {
        super(message);
    }
}
