package ee.ut.banking.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "accounts")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class,
                  property = "id")
public class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, unique = true)
    @JsonProperty
    private long id;

    @NotNull(message = "Please provide an account name")
    @Column(name = "name", nullable = false, unique = true)
    @JsonProperty(required = true)
    private String name;

    @NotNull(message = "Please provide a starting balance")
    @Column(name = "balance", nullable = false)
    @JsonProperty(required = true)
    private int balance;

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "owner")
    @JsonProperty
    private Code activeCode;

    @ManyToMany(mappedBy = "accounts")
    @JsonProperty(required = true)
    @JsonIdentityReference(alwaysAsId = true)
    private Set<User> users;

    @OneToMany(
            mappedBy = "fromAccount",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    @JsonProperty(required = true)
    @JsonIdentityReference(alwaysAsId = true)
    private Set<Transaction> sentTransactions;

    @OneToMany(
            mappedBy = "toAccount",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    @JsonProperty(required = true)
    @JsonIdentityReference(alwaysAsId = true)
    private Set<Transaction> receivedTransactions;

    private static final String PROPERTY_id = "id";
    private static final String PROPERTY_name = "name";
    private static final String PROPERTY_balance = "balance";
    private static final String PROPERTY_users = "users";
    private static final String PROPERTY_activeCode = "activeCode";
    private static final String PROPERTY_sentTransactions = "sentTransactions";
    private static final String PROPERTY_receivedTransactions = "receivedTransactions";
    @Transient
    protected PropertyChangeSupport listeners;

    public long getId() {
        return this.id;
    }

    public Account setId(long value) {
        if (value == this.id) {
            return this;
        }

        final long oldValue = this.id;
        this.id = value;
        this.firePropertyChange(PROPERTY_id, oldValue, value);
        return this;
    }

    public String getName()
    {
        return this.name;
    }

    public Account setName(String value)
    {
        if (value.equals(this.name)) {
            return this;
        }

        final String oldValue = this.name;
        this.name = value;
        this.firePropertyChange(PROPERTY_name, oldValue, value);
        return this;
    }

    public int getBalance() {
        return this.balance;
    }

    public Account setBalance(int value) {
        if (value == this.balance) {
            return this;
        }

        final int oldValue = this.balance;
        this.balance = value;
        this.firePropertyChange(PROPERTY_balance, oldValue, value);
        return this;
    }

    public Set<User> getUsers() {
        return this.users != null ? Collections.unmodifiableSet(this.users) : Collections.emptySet();
    }

    public Account withUsers(User value) {
        if (this.users == null) {
            this.users = new HashSet<>();
        }
        if (!this.users.contains(value)) {
            this.users.add(value);
            value.withAccounts(this);
            this.firePropertyChange(PROPERTY_users, null, value);
        }
        return this;
    }

    public Account withUsers(User... value) {
        for (final User item : value) {
            this.withUsers(item);
        }
        return this;
    }

    public Account withUsers(Collection<? extends User> value) {
        for (final User item : value) {
            this.withUsers(item);
        }
        return this;
    }

    public Account withoutUsers(User value) {
        if (this.users != null && this.users.remove(value)) {
            value.withoutAccounts(this);
            this.firePropertyChange(PROPERTY_users, value, null);
        }
        return this;
    }

    public Account withoutUsers(User... value) {
        for (final User item : value) {
            this.withoutUsers(item);
        }
        return this;
    }

    public Account withoutUsers(Collection<? extends User> value) {
        for (final User item : value) {
            this.withoutUsers(item);
        }
        return this;
    }

    public Set<Transaction> getSentTransactions() {
        return this.sentTransactions != null ? Collections.unmodifiableSet(this.sentTransactions)
                                             : Collections.emptySet();
    }

    public Account withSentTransactions(Transaction value) {
        if (this.sentTransactions == null) {
            this.sentTransactions = new HashSet<>();
        }
        if (!this.sentTransactions.contains(value)) {
            this.sentTransactions.add(value);
            value.setFromAccount(this);
            this.firePropertyChange(PROPERTY_sentTransactions, null, value);
        }
        return this;
    }

    public Account withSentTransactions(Transaction... value) {
        for (final Transaction item : value) {
            this.withSentTransactions(item);
        }
        return this;
    }

    public Account withSentTransactions(Collection<? extends Transaction> value) {
        for (final Transaction item : value) {
            this.withSentTransactions(item);
        }
        return this;
    }

    public Account withoutSentTransactions(Transaction value) {
        if (this.sentTransactions != null && this.sentTransactions.remove(value)) {
            value.setFromAccount(null);
            this.firePropertyChange(PROPERTY_sentTransactions, value, null);
        }
        return this;
    }

    public Account withoutSentTransactions(Transaction... value) {
        for (final Transaction item : value) {
            this.withoutSentTransactions(item);
        }
        return this;
    }

    public Account withoutSentTransactions(Collection<? extends Transaction> value) {
        for (final Transaction item : value) {
            this.withoutSentTransactions(item);
        }
        return this;
    }

    public Set<Transaction> getReceivedTransactions() {
        return this.receivedTransactions != null ? Collections.unmodifiableSet(this.receivedTransactions)
                                                 : Collections.emptySet();
    }

    public Account withReceivedTransactions(Transaction value) {
        if (this.receivedTransactions == null) {
            this.receivedTransactions = new HashSet<>();
        }
        if (!this.receivedTransactions.contains(value)) {
            this.receivedTransactions.add(value);
            value.setToAccount(this);
            this.firePropertyChange(PROPERTY_receivedTransactions, null, value);
        }
        return this;
    }

    public Account withReceivedTransactions(Transaction... value) {
        for (final Transaction item : value) {
            this.withReceivedTransactions(item);
        }
        return this;
    }

    public Account withReceivedTransactions(Collection<? extends Transaction> value) {
        for (final Transaction item : value) {
            this.withReceivedTransactions(item);
        }
        return this;
    }

    public Account withoutReceivedTransactions(Transaction value) {
        if (this.receivedTransactions != null && this.receivedTransactions.remove(value)) {
            value.setToAccount(null);
            this.firePropertyChange(PROPERTY_receivedTransactions, value, null);
        }
        return this;
    }

    public Account withoutReceivedTransactions(Transaction... value) {
        for (final Transaction item : value) {
            this.withoutReceivedTransactions(item);
        }
        return this;
    }

    public Account withoutReceivedTransactions(Collection<? extends Transaction> value) {
        for (final Transaction item : value) {
            this.withoutReceivedTransactions(item);
        }
        return this;
    }

    public Code getActiveCode() {
        return this.activeCode;
    }

    public Account setActiveCode(Code value) {
        if (this.activeCode == value) {
            return this;
        }

        final Code oldValue = this.activeCode;
        if (this.activeCode != null) {
            this.activeCode = null;
            oldValue.setOwner(null);
        }
        this.activeCode = value;
        if (value != null) {
            value.setOwner(this);
        }
        this.firePropertyChange(PROPERTY_activeCode, oldValue, value);
        return this;
    }

    public boolean firePropertyChange(String propertyName, Object oldValue, Object newValue) {
        if (this.listeners != null) {
            this.listeners.firePropertyChange(propertyName, oldValue, newValue);
            return true;
        }
        return false;
    }

    public boolean addPropertyChangeListener(PropertyChangeListener listener) {
        if (this.listeners == null) {
            this.listeners = new PropertyChangeSupport(this);
        }
        this.listeners.addPropertyChangeListener(listener);
        return true;
    }

    public boolean addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        if (this.listeners == null) {
            this.listeners = new PropertyChangeSupport(this);
        }
        this.listeners.addPropertyChangeListener(propertyName, listener);
        return true;
    }

    public boolean removePropertyChangeListener(PropertyChangeListener listener) {
        if (this.listeners != null) {
            this.listeners.removePropertyChangeListener(listener);
        }
        return true;
    }

    public boolean removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        if (this.listeners != null) {
            this.listeners.removePropertyChangeListener(propertyName, listener);
        }
        return true;
    }

    public void removeYou() {
        this.withoutUsers(new HashSet<>(this.getUsers()));
        this.withoutSentTransactions(new HashSet<>(this.getSentTransactions()));
        this.withoutReceivedTransactions(new HashSet<>(this.getReceivedTransactions()));
        this.setActiveCode(null);
    }
}