package ee.ut.banking.model;

import com.fasterxml.jackson.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "transactions")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class,
                  property = "id")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Transaction {
    private static final String PROPERTY_id = "id";
    private static final String PROPERTY_amount = "amount";
    private static final String PROPERTY_date = "date";
    private static final String PROPERTY_status = "status";
    private static final String PROPERTY_fromAccount = "fromAccount";
    private static final String PROPERTY_toAccount = "toAccount";
    private static final String PROPERTY_fromTransaction = "fromTransaction";
    private static final String PROPERTY_toTransaction = "toTransaction";
    @Transient
    protected PropertyChangeSupport listeners;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true)
    private long id;

    @NotNull(message = "Please provide an amount")
    @Column(name = "amount")
    @JsonProperty(required = true)
    private int amount;

    @GeneratedValue
    @CreationTimestamp
    @Column(name = "date")
    @JsonProperty(required = true)
    private LocalDateTime date;

    @Column(name = "status")
    @JsonProperty(required = true)
    private String status;

    @ManyToOne
    @JsonIgnore
    private Account fromAccount;

    @NotNull(message = "Please provide an amount")
    @ManyToOne
    @JsonIgnore
    private Account toAccount;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "from_transaction_id", referencedColumnName = "id")
    @JsonProperty
    @JsonIdentityReference(alwaysAsId = true)
    private Transaction fromTransaction;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "to_transaction_id", referencedColumnName = "id")
    @JsonProperty
    @JsonIdentityReference(alwaysAsId = true)
    private Transaction toTransaction;

    @JsonProperty("toAccount")
    public String getToAccountName() {
        return toAccount == null ? "" : toAccount.getName();
    }

    @JsonProperty("fromAccount")
    public String getFromAccountName() {
        return fromAccount == null ? "" : fromAccount.getName();
    }

    public int getAmount() {
        return this.amount;
    }

    public Transaction setAmount(int value) {
        if (value == this.amount) {
            return this;
        }

        final int oldValue = this.amount;
        this.amount = value;
        this.firePropertyChange(PROPERTY_amount, oldValue, value);
        return this;
    }

    public LocalDateTime getDate() {
        return this.date;
    }

    public Transaction setDate(LocalDateTime value) {
        if (Objects.equals(value, this.date)) {
            return this;
        }

        final LocalDateTime oldValue = this.date;
        this.date = value;
        this.firePropertyChange(PROPERTY_date, oldValue, value);
        return this;
    }

    public String getStatus() {
        return this.status;
    }

    public Transaction setStatus(String value) {
        if (Objects.equals(value, this.status)) {
            return this;
        }

        final String oldValue = this.status;
        this.status = value;
        this.firePropertyChange(PROPERTY_status, oldValue, value);
        return this;
    }

    public Account getFromAccount() {
        return this.fromAccount;
    }

    public Transaction setFromAccount(Account value) {
        if (this.fromAccount == value) {
            return this;
        }

        final Account oldValue = this.fromAccount;
        if (this.fromAccount != null) {
            this.fromAccount = null;
            oldValue.withoutSentTransactions(this);
        }
        this.fromAccount = value;
        if (value != null) {
            value.withSentTransactions(this);
        }
        this.firePropertyChange(PROPERTY_fromAccount, oldValue, value);
        return this;
    }

    public Account getToAccount() {
        return this.toAccount;
    }

    public Transaction setToAccount(Account value) {
        if (this.toAccount == value) {
            return this;
        }

        final Account oldValue = this.toAccount;
        if (this.toAccount != null) {
            this.toAccount = null;
            oldValue.withoutReceivedTransactions(this);
        }
        this.toAccount = value;
        if (value != null) {
            value.withReceivedTransactions(this);
        }
        this.firePropertyChange(PROPERTY_toAccount, oldValue, value);
        return this;
    }

    public Transaction getFromTransaction() {
        return this.fromTransaction;
    }

    public Transaction setFromTransaction(Transaction value) {
        if (this.fromTransaction == value) {
            return this;
        }

        final Transaction oldValue = this.fromTransaction;
        if (this.fromTransaction != null) {
            this.fromTransaction = null;
            oldValue.setToTransaction(null);
        }
        this.fromTransaction = value;
        if (value != null) {
            value.setToTransaction(this);
        }
        this.firePropertyChange(PROPERTY_fromTransaction, oldValue, value);
        return this;
    }

    public Transaction getToTransaction() {
        return this.toTransaction;
    }

    public Transaction setToTransaction(Transaction value) {
        if (this.toTransaction == value) {
            return this;
        }

        final Transaction oldValue = this.toTransaction;
        if (this.toTransaction != null) {
            this.toTransaction = null;
            oldValue.setFromTransaction(null);
        }
        this.toTransaction = value;
        if (value != null) {
            value.setFromTransaction(this);
        }
        this.firePropertyChange(PROPERTY_toTransaction, oldValue, value);
        return this;
    }

    public boolean firePropertyChange(String propertyName, Object oldValue, Object newValue) {
        if (this.listeners != null) {
            this.listeners.firePropertyChange(propertyName, oldValue, newValue);
            return true;
        }
        return false;
    }

    public boolean addPropertyChangeListener(PropertyChangeListener listener) {
        if (this.listeners == null) {
            this.listeners = new PropertyChangeSupport(this);
        }
        this.listeners.addPropertyChangeListener(listener);
        return true;
    }

    public boolean addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        if (this.listeners == null) {
            this.listeners = new PropertyChangeSupport(this);
        }
        this.listeners.addPropertyChangeListener(propertyName, listener);
        return true;
    }

    public boolean removePropertyChangeListener(PropertyChangeListener listener) {
        if (this.listeners != null) {
            this.listeners.removePropertyChangeListener(listener);
        }
        return true;
    }

    public boolean removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        if (this.listeners != null) {
            this.listeners.removePropertyChangeListener(propertyName, listener);
        }
        return true;
    }

    @Override
    public String toString() {
        final StringBuilder result = new StringBuilder();
        result.append(' ').append(this.getStatus());
        return result.substring(1);
    }

    public void removeYou() {
        this.setFromAccount(null);
        this.setToAccount(null);
        this.setFromTransaction(null);
        this.setToTransaction(null);
    }

    public long getId() {
        return this.id;
    }

    public Transaction setId(long value) {
        if (value == this.id) {
            return this;
        }

        final long oldValue = this.id;
        this.id = value;
        this.firePropertyChange(PROPERTY_id, oldValue, value);
        return this;
    }
}
