package ee.ut.banking.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Objects;

@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "codes")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class,
                  property = "id")
public class Code {

    private static final String PROPERTY_id = "id";
    private static final String PROPERTY_amount = "amount";
    private static final String PROPERTY_value = "value";
    private static final String PROPERTY_owner = "owner";
    @Transient
    protected PropertyChangeSupport listeners;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, unique = true)
    private long id;

    @NotNull(message = "Please provide an amount")
    @Column(name = "amount")
    @JsonProperty(required = true)
    private int amount;

    @NotNull(message = "Please provide a value")
    @Column(name = "value", unique = true)
    @JsonProperty(required = true)
    private String value;

    @NotNull(message = "Please provide a owner for code")
    @OneToOne(cascade = CascadeType.ALL)
    @JsonIgnore
    private Account owner;

    @JsonProperty(value = "owner")
    public String getOwnerName() {
        return owner == null ? "" : owner.getName();
    }

    public long getId() {
        return this.id;
    }

    public Code setId(long value) {
        if (value == this.id) {
            return this;
        }

        final long oldValue = this.id;
        this.id = value;
        this.firePropertyChange(PROPERTY_id, oldValue, value);
        return this;
    }

    public int getAmount() {
        return this.amount;
    }

    public Code setAmount(int value) {
        if (value == this.amount) {
            return this;
        }

        final int oldValue = this.amount;
        this.amount = value;
        this.firePropertyChange(PROPERTY_amount, oldValue, value);
        return this;
    }

    public String getValue() {
        return this.value;
    }

    public Code setValue(String value) {
        if (Objects.equals(value, this.value)) {
            return this;
        }

        final String oldValue = this.value;
        this.value = value;
        this.firePropertyChange(PROPERTY_value, oldValue, value);
        return this;
    }

    public Account getOwner() {
        return this.owner;
    }

    public Code setOwner(Account value) {
        if (this.owner == value) {
            return this;
        }

        final Account oldValue = this.owner;
        if (this.owner != null) {
            this.owner = null;
            oldValue.setActiveCode(null);
        }
        this.owner = value;
        if (value != null) {
            value.setActiveCode(this);
        }
        this.firePropertyChange(PROPERTY_owner, oldValue, value);
        return this;
    }

    public boolean firePropertyChange(String propertyName, Object oldValue, Object newValue) {
        if (this.listeners != null) {
            this.listeners.firePropertyChange(propertyName, oldValue, newValue);
            return true;
        }
        return false;
    }

    public boolean addPropertyChangeListener(PropertyChangeListener listener) {
        if (this.listeners == null) {
            this.listeners = new PropertyChangeSupport(this);
        }
        this.listeners.addPropertyChangeListener(listener);
        return true;
    }

    public boolean addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        if (this.listeners == null) {
            this.listeners = new PropertyChangeSupport(this);
        }
        this.listeners.addPropertyChangeListener(propertyName, listener);
        return true;
    }

    public boolean removePropertyChangeListener(PropertyChangeListener listener) {
        if (this.listeners != null) {
            this.listeners.removePropertyChangeListener(listener);
        }
        return true;
    }

    public boolean removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        if (this.listeners != null) {
            this.listeners.removePropertyChangeListener(propertyName, listener);
        }
        return true;
    }

    @Override
    public String toString() {
        final StringBuilder result = new StringBuilder();
        result.append(' ').append(this.getValue());
        return result.substring(1);
    }

    public void removeYou() {
        this.setOwner(null);
    }
}