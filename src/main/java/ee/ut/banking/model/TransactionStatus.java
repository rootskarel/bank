package ee.ut.banking.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum TransactionStatus {
    REVOKED("REVOKED"),
    MODIFIED("MODIFIED"),
    ACTIVE("ACTIVE");

    private final String value;
}
