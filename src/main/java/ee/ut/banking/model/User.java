package ee.ut.banking.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.*;

@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "users")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class,
                  property = "id")
public class User {
    private static final String PROPERTY_id = "id";
    private static final String PROPERTY_name = "name";
    private static final String PROPERTY_username = "username";
    private static final String PROPERTY_password = "password";
    private static final String PROPERTY_accounts = "accounts";
    private static final String PROPERTY_role = "role";
    @Transient
    protected PropertyChangeSupport listeners;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, unique = true)
    private long id;

    @NotNull(message = "Please provide a name")
    @Column(name = "name", nullable = false)
    @JsonProperty(required = true)
    private String name;

    @NotNull(message = "Please provide a username")
    @Column(name = "username", nullable = false, unique = true)
    @JsonProperty(required = true)
    private String username;

    @NotNull(message = "Please provide a password")
    @Column(name = "password", nullable = false)
    private String password;

    @Column(name = "role", nullable = false)
    private String role;

    @ManyToMany
    @JoinTable(
            name = "has_account",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "account_id"))
    private Set<Account> accounts = new HashSet<>();

    public String getName() {
        return this.name;
    }

    public User setName(String value) {
        if (Objects.equals(value, this.name)) {
            return this;
        }

        final String oldValue = this.name;
        this.name = value;
        this.firePropertyChange(PROPERTY_name, oldValue, value);
        return this;
    }

    public String getUsername() {
        return this.username;
    }

    public User setUsername(String value) {
        if (Objects.equals(value, this.username)) {
            return this;
        }

        final String oldValue = this.username;
        this.username = value;
        this.firePropertyChange(PROPERTY_username, oldValue, value);
        return this;
    }

    public String getPassword() {
        return this.password;
    }

    public User setPassword(String value) {
        if (Objects.equals(value, this.password)) {
            return this;
        }

        final String oldValue = this.password;
        this.password = value;
        this.firePropertyChange(PROPERTY_password, oldValue, value);
        return this;
    }

    public String getRole() {
        return this.role;
    }

    public User setRole(String value) {
        if (Objects.equals(value, this.role)) {
            return this;
        }

        final String oldValue = this.role;
        this.role = value;
        this.firePropertyChange(PROPERTY_role, oldValue, value);
        return this;
    }

    public Set<Account> getAccounts() {
        return this.accounts != null ? Collections.unmodifiableSet(this.accounts) : Collections.emptySet();
    }

    public User withAccounts(Account value) {
        if (this.accounts == null) {
            this.accounts = new HashSet<>();
        }
        if (!this.accounts.contains(value)) {
            this.accounts.add(value);
            value.withUsers(this);
            this.firePropertyChange(PROPERTY_accounts, null, value);
        }

        return this;
    }

    public User withAccounts(Account... value) {
        for (final Account item : value) {
            this.withAccounts(item);
        }
        return this;
    }

    public User withAccounts(Collection<? extends Account> value) {
        for (final Account item : value) {
            this.withAccounts(item);
        }
        return this;
    }

    public User withoutAccounts(Account value) {
        if (this.accounts != null && this.accounts.remove(value)) {
            value.withoutUsers(this);
            this.firePropertyChange(PROPERTY_accounts, value, null);
        }
        return this;
    }

    public User withoutAccounts(Account... value) {
        for (final Account item : value) {
            this.withoutAccounts(item);
        }
        return this;
    }

    public User withoutAccounts(Collection<? extends Account> value) {
        for (final Account item : value) {
            this.withoutAccounts(item);
        }
        return this;
    }

    public boolean firePropertyChange(String propertyName, Object oldValue, Object newValue) {
        if (this.listeners != null) {
            this.listeners.firePropertyChange(propertyName, oldValue, newValue);
            return true;
        }
        return false;
    }

    public boolean addPropertyChangeListener(PropertyChangeListener listener) {
        if (this.listeners == null) {
            this.listeners = new PropertyChangeSupport(this);
        }
        this.listeners.addPropertyChangeListener(listener);
        return true;
    }

    public boolean addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        if (this.listeners == null) {
            this.listeners = new PropertyChangeSupport(this);
        }
        this.listeners.addPropertyChangeListener(propertyName, listener);
        return true;
    }

    public boolean removePropertyChangeListener(PropertyChangeListener listener) {
        if (this.listeners != null) {
            this.listeners.removePropertyChangeListener(listener);
        }
        return true;
    }

    public boolean removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        if (this.listeners != null) {
            this.listeners.removePropertyChangeListener(propertyName, listener);
        }
        return true;
    }

    @Override
    public String toString() {
        final StringBuilder result = new StringBuilder();
        result.append(' ').append(this.getName());
        result.append(' ').append(this.getUsername());
        result.append(' ').append(this.getPassword());
        result.append(' ').append(this.getRole());
        return result.substring(1);
    }

    public void removeYou() {
        this.withoutAccounts(new HashSet<>(this.getAccounts()));
    }

    public long getId() {
        return this.id;
    }

    public User setId(long value) {
        if (value == this.id) {
            return this;
        }

        final long oldValue = this.id;
        this.id = value;
        this.firePropertyChange(PROPERTY_id, oldValue, value);
        return this;
    }
}
