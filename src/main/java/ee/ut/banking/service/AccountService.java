package ee.ut.banking.service;

import ee.ut.banking.model.Account;
import ee.ut.banking.repository.AccountRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AccountService {

    private final AccountRepository accountRepository;

    public void saveAccount(Account account) {
        accountRepository.save(account);
    }

    public Account getAccountByName(String name) {
        return accountRepository.findByName(name);
    }
}
