package ee.ut.banking.service;

import ee.ut.banking.controller.domain.CodeRequest;
import ee.ut.banking.exception.ConstraintException;
import ee.ut.banking.model.Account;
import ee.ut.banking.model.Code;
import ee.ut.banking.repository.CodeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CodeService {

    private final CodeRepository codeRepository;
    private final AccountService accountService;

    public List<Code> getAllCodes() {
        return codeRepository.findAll();
    }

    public Code getCodeByValue(String value) throws ConstraintException {
        var code = codeRepository.findByValue(value);
        if (code == null) {
            throw new ConstraintException("Invalid code value.");
        }
        return codeRepository.findByValue(value);
    }

    public Code getCodeByOwner(Account owner) {
        return codeRepository.findByOwner(owner);
    }

    public Code createCode(CodeRequest codeRequest) throws ConstraintException {
        if (codeRequest.getAmount() < 1) {
            throw new ConstraintException("Invalid amount.");
        }

        var owner = accountService.getAccountByName(codeRequest.getToAccountName());
        if (owner == null) {
            throw new ConstraintException("Invalid account.");
        }

        var code = getCodeByOwner(owner);
        if (code == null) {
            code = Code.builder()
                       .owner(owner)
                       .build();
        }
        code.setAmount(codeRequest.getAmount());

        var currentCodes = getAllCodes().stream()
                                        .map(Code::getValue)
                                        .collect(Collectors.toList());
        var generatedCode = generateCode();
        while (currentCodes.contains(generatedCode)) {
            generatedCode = generateCode();
        }
        code.setValue(generatedCode);
        return codeRepository.save(code);
    }

    public String generateCode() {
        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = 5;
        Random random = new Random();
        StringBuilder buffer = new StringBuilder(targetStringLength);
        for (int i = 0; i < targetStringLength; i++) {
            int randomLimitedInt = leftLimit + (int)
                    (random.nextFloat() * (rightLimit - leftLimit + 1));
            buffer.append((char) randomLimitedInt);
        }
        return buffer.toString().toUpperCase();
    }
}
