package ee.ut.banking.service;

import ee.ut.banking.controller.domain.CodeTransactionRequest;
import ee.ut.banking.controller.domain.SeedTransactionRequest;
import ee.ut.banking.controller.domain.TransactionRequest;
import ee.ut.banking.controller.domain.TransactionRevokeRequest;
import ee.ut.banking.exception.ConstraintException;
import ee.ut.banking.model.Transaction;
import ee.ut.banking.model.TransactionStatus;
import ee.ut.banking.repository.TransactionRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class TransactionService {

    private static final String BANK_VAULT_ACCOUNT_NAME = "BANK VAULT";

    private final TransactionRepository transactionRepository;
    private final AccountService accountService;
    private final CodeService codeService;

    public List<Transaction> getAllTransactions() {
        return transactionRepository.findAll();
    }

    public Transaction getTransaction(long id) {
        return transactionRepository.findById(id)
                                    .orElse(null);
    }

    public Transaction makeTransaction(TransactionRequest request) throws ConstraintException {
        var fromAccountName = request.getFromAccountName();
        var toAccountName = request.getToAccountName();

        if (fromAccountName.equals(toAccountName)) {
            throw new ConstraintException("Sender and receiver cannot be the same");
        }

        var sender = accountService.getAccountByName(fromAccountName);
        var receiver = accountService.getAccountByName(toAccountName);

        if (sender == null) {
            throw new ConstraintException("Invalid sender.");
        }
        if (receiver == null) {
            throw new ConstraintException("Invalid receiver.");
        }
        if (request.getAmount() < 1) {
            throw new ConstraintException("Amount has to be higher than 0");
        }

        var transaction = Transaction.builder()
                                     .fromAccount(sender)
                                     .toAccount(receiver)
                                     .amount(request.getAmount())
                                     .status(TransactionStatus.ACTIVE.getValue())
                                     .build();

        return transactionRepository.save(modifyBalance(transaction));
    }

    private Transaction findLatestTransaction(Transaction transaction) {
        var currentTransaction = transaction;
        while (currentTransaction.getToTransaction() != null) {
            currentTransaction = currentTransaction.getToTransaction();
        }
        return currentTransaction;
    }

    public Transaction revokeTransaction(TransactionRevokeRequest request) throws ConstraintException {
        var originalTransaction = transactionRepository.findById(request.getId())
                                                       .orElseThrow(() -> new ConstraintException("Invalid transaction id"));
        var latestTransaction = findLatestTransaction(originalTransaction);
        if (!latestTransaction.getStatus()
                              .equals(TransactionStatus.ACTIVE.getValue())) {
            throw new ConstraintException("Only active transactions can be revoked");
        }
        var newTransaction = Transaction.builder()
                                        .fromAccount(latestTransaction.getToAccount())
                                        .toAccount(latestTransaction.getFromAccount())
                                        .amount(latestTransaction.getAmount())
                                        .fromTransaction(latestTransaction)
                                        .status(TransactionStatus.ACTIVE.getValue())
                                        .build();
        latestTransaction.setToTransaction(newTransaction);
        latestTransaction.setStatus(TransactionStatus.REVOKED.getValue());

        modifyBalance(newTransaction);
        transactionRepository.save(newTransaction);
        return newTransaction;
    }

    private Transaction modifyBalance(Transaction transaction) throws ConstraintException {
        int fromAccountBalance = transaction.getFromAccount()
                                            .getBalance();
        int transactionAmount = transaction.getAmount();
        if (fromAccountBalance < transactionAmount) {
            throw new ConstraintException(String.format("Not enough balance (current: %d) for transaction amount %d",
                                                        fromAccountBalance,
                                                        transactionAmount));
        } else {
            transaction.getFromAccount()
                       .setBalance(fromAccountBalance - transactionAmount);
            transaction.getToAccount()
                       .setBalance(transaction.getToAccount()
                                              .getBalance() + transactionAmount);
            return transaction;
        }
    }

    public Integer makeCodeTransaction(CodeTransactionRequest codeTransaction) throws ConstraintException {
        var fromAccountName = codeTransaction.getFromAccountName();
        var code = codeService.getCodeByValue(codeTransaction.getCodeValue());
        if (code == null) {
            throw new ConstraintException("Invalid transaction code.");
        }
        var toAccountName = code.getOwner()
                                .getName();

        var transactionRequest = TransactionRequest.builder()
                                                   .fromAccountName(fromAccountName)
                                                   .toAccountName(toAccountName)
                .amount(code.getAmount())
                .build();
        return makeTransaction(transactionRequest).getFromAccount()
                                                  .getBalance();
    }

    public Transaction modifyTransaction(TransactionRequest request) throws ConstraintException {
        var newAmount = request.getAmount();
        if (newAmount < 1) {
            throw new ConstraintException("Amount has to be higher than 0");
        }
        var originalTransaction = transactionRepository.findById(request.getId())
                                                       .orElseThrow(() -> new ConstraintException("Invalid transaction id"));
        var latestTransaction = findLatestTransaction(originalTransaction);

        if (!latestTransaction.getStatus()
                              .equals(TransactionStatus.ACTIVE.getValue())) {
            throw new ConstraintException("Only active transactions can be modified");
        }

        var newFromAccount = accountService.getAccountByName(request.getFromAccountName());
        var newToAccount = accountService.getAccountByName(request.getToAccountName());

        if (newFromAccount == null) {
            throw new ConstraintException("Invalid sender account");
        }

        if (newToAccount == null) {
            throw new ConstraintException("Invalid receiver account");
        }

        if (newFromAccount.getName()
                          .equals(newToAccount.getName())) {
            throw new ConstraintException("Sender and receiver cannot be the same");
        }

        var oldAmount = latestTransaction.getAmount();
        var oldFrom = latestTransaction.getFromAccount();
        var oldTo = latestTransaction.getToAccount();

        var isToSame = latestTransaction.getToAccount() == newToAccount;
        var isFromSame = latestTransaction.getFromAccount() == newFromAccount;

        if (latestTransaction.getFromAccount() == newFromAccount
            && latestTransaction.getToAccount() == newToAccount
            && latestTransaction.getAmount() == newAmount) {
            throw new ConstraintException("Nothing has been modified.");
        }
        var returnTransaction = Transaction.builder()
                                        .fromAccount(oldTo)
                                        .toAccount(oldFrom)
                                        .amount(oldAmount)
                                        .build();

        var newTransaction = Transaction.builder()
                                        .fromAccount(newFromAccount)
                                        .toAccount(newToAccount)
                                        .amount(newAmount)
                                        .fromTransaction(latestTransaction)
                                        .status(TransactionStatus.ACTIVE.getValue())
                                        .build();

        // Receiver and Sender same -> Only one balance modification (in case not enough money in-between transfers)
        if (isFromSame && isToSame) {
            newTransaction.setAmount(newAmount - oldAmount);
            modifyBalance(newTransaction);
            newTransaction.setAmount(newAmount);
        }
        // Receiver same -> New transaction first (in case not enough money to return old)
        else if (isFromSame) {
            modifyBalance(newTransaction);
            modifyBalance(returnTransaction);
        }
        // Sender same -> Return old transaction first (in case not enough money to make new)
        // Or else sequence does not matter
        else {
            modifyBalance(returnTransaction);
            modifyBalance(newTransaction);
        }
        latestTransaction.setToTransaction(newTransaction);
        latestTransaction.setStatus(TransactionStatus.MODIFIED.getValue());
        transactionRepository.save(newTransaction);
        return newTransaction;
    }

    public Transaction seedTransaction(SeedTransactionRequest request) throws ConstraintException {
        var receiver = accountService.getAccountByName(request.getToAccountName());
        var bankVaultAccount = accountService.getAccountByName(BANK_VAULT_ACCOUNT_NAME);

        if (receiver == null) {
            throw new ConstraintException("Invalid receiver.");
        }

        if (bankVaultAccount == null) {
            throw new ConstraintException("Bank vault account not found.");
        }

        if (request.getAmount() <= 0) {
            throw new ConstraintException("Amount has to be higher than 0");
        }

        bankVaultAccount.setBalance(bankVaultAccount.getBalance() - request.getAmount());
        receiver.setBalance(receiver.getBalance() + request.getAmount());

        accountService.saveAccount(receiver);

        var transaction = Transaction.builder()
                                     .fromAccount(bankVaultAccount)
                                     .toAccount(receiver)
                                     .amount(request.getAmount())
                                     .status(TransactionStatus.ACTIVE.getValue())
                                     .build();

        return transactionRepository.save(transaction);
    }
}