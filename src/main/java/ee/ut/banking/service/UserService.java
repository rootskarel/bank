package ee.ut.banking.service;

import ee.ut.banking.controller.domain.UserRequest;
import ee.ut.banking.exception.ConstraintException;
import ee.ut.banking.model.Account;
import ee.ut.banking.model.User;
import ee.ut.banking.repository.UserRepository;
import ee.ut.banking.security.domain.UserRole;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.passay.CharacterData;
import org.passay.CharacterRule;
import org.passay.EnglishCharacterData;
import org.passay.PasswordGenerator;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.beans.TypeMismatchException.ERROR_CODE;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;
    private final PasswordGenerator passwordGen;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final AccountService accountService;
    private final CodeService codeService;

    public List<User> getAllUsers() {
        return userRepository.findAll()
                             .stream()
                             .map(user -> user.setPassword(null))
                             .collect(Collectors.toList());
    }

    public User getUserByUsername(@NonNull String username) {
        var user = userRepository.findByUsername(username);
        if (user != null) {
            user.setPassword(null);
        }
        return user;
    }

    public User getUserByAccountName(@NonNull String accountName) throws ConstraintException {
        var account = accountService.getAccountByName(accountName);
        if (account == null) {
            throw new ConstraintException("Invalid account name.");
        }
        return account.getUsers()
                      .stream()
                      .findFirst()
                      .orElseThrow(() -> new ConstraintException("This account does not have a user."));
    }

    public User getUserByCodeValue(@NonNull String codeValue) throws ConstraintException {
        var code = codeService.getCodeByValue(codeValue);
        if (code == null) {
            throw new ConstraintException("Invalid code value.");
        }
        var accountName = code.getOwnerName();
        return getUserByAccountName(accountName);
    }

    private User saveUser(User user) throws ConstraintException {
        if (user.getName() == null) {
            throw new ConstraintException("Name is invalid.");
        }
        return userRepository.save(user);
    }

    private boolean accountExists(@NonNull String accountName) {
        return accountService.getAccountByName(accountName) != null;
    }

    private boolean userExists(@NonNull String username) {
        return userRepository.findByUsername(username) != null;
    }

    public User createUser(UserRequest req) throws ConstraintException {
        if (userExists(req.getUsername())) {
            throw new ConstraintException("User with username already exists");
        } else if (accountExists(req.getAccountName())) {
            throw new ConstraintException("Account with given name already exists");
        }

        var password = generatePassword();
        var encodedPassword = bCryptPasswordEncoder.encode(password);
        var newUser = User.builder()
                          .name(req.getName())
                          .username(req.getUsername())
                          .password(encodedPassword)
                          .role(UserRole.USER.getValue())
                          .build();
        var newAccount = Account.builder()
                                .name(req.getAccountName())
                                .balance(0)
                                .build();

        newUser.withAccounts(newAccount);
        accountService.saveAccount(newAccount);

        var savedUser = saveUser(newUser);
        // return plain-text password
        savedUser.setPassword(password);
        return savedUser;
    }

    private String generatePassword() {
        CharacterData lowerCaseChars = EnglishCharacterData.LowerCase;
        CharacterRule lowerCaseRule = new CharacterRule(lowerCaseChars);
        lowerCaseRule.setNumberOfCharacters(4);

        CharacterData upperCaseChars = EnglishCharacterData.UpperCase;
        CharacterRule upperCaseRule = new CharacterRule(upperCaseChars);
        upperCaseRule.setNumberOfCharacters(2);

        CharacterData digitChars = EnglishCharacterData.Digit;
        CharacterRule digitRule = new CharacterRule(digitChars);
        digitRule.setNumberOfCharacters(2);

        CharacterData specialChars = new CharacterData() {
            public String getErrorCode() {
                return ERROR_CODE;
            }

            public String getCharacters() {
                return "!@#$%^&*()_+";
            }
        };
        CharacterRule splCharRule = new CharacterRule(specialChars);
        splCharRule.setNumberOfCharacters(2);

        return passwordGen.generatePassword(12, splCharRule, lowerCaseRule, upperCaseRule, digitRule);
    }
}
