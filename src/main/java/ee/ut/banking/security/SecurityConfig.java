package ee.ut.banking.security;

import ee.ut.banking.security.domain.UserRole;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    private final DataSource dataSource;

    @Value("${spring.queries.users-query}")
    private String usersQuery;

    @Value("${spring.queries.roles-query}")
    private String rolesQuery;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.jdbcAuthentication()
            .usersByUsernameQuery(usersQuery)
            .authoritiesByUsernameQuery(rolesQuery)
            .dataSource(dataSource)
            .passwordEncoder(bCryptPasswordEncoder);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        var adminRole = UserRole.ADMIN.getValue();
        http.csrf()
            .disable()
            .authorizeRequests()
            .antMatchers(HttpMethod.OPTIONS, "/**")
            .permitAll()
            .antMatchers("/h2**")
            .permitAll()
            .antMatchers("/user/all")
            .permitAll()
            .antMatchers("/user/by-account")
            .permitAll()
            .antMatchers("/user/by-code")
            .permitAll()
            .antMatchers("/code/by-value")
            .permitAll()
            .antMatchers("/user/new")
            .hasAuthority(adminRole)
            .antMatchers("/transaction/new")
            .hasAuthority(adminRole)
            .antMatchers("/transaction/{id}")
            .hasAuthority(adminRole)
            .antMatchers("/transaction/all")
            .hasAuthority(adminRole)
            .antMatchers("/transaction/revoke")
            .hasAuthority(adminRole)
            .antMatchers("/transaction/modify")
            .hasAuthority(adminRole)
            .antMatchers("/transaction/seed")
            .hasAuthority(adminRole)
            .anyRequest()
            .authenticated()
            .and()
            .httpBasic()
            .and()
            .logout()
            .logoutUrl("/logout")
            .invalidateHttpSession(true)
            .deleteCookies("user")
            .deleteCookies("JSESSIONID");

        // To fix h2 console
        http.headers().frameOptions().disable();
    }

}