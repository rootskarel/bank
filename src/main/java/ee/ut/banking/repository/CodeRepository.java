package ee.ut.banking.repository;

import ee.ut.banking.model.Account;
import ee.ut.banking.model.Code;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CodeRepository extends JpaRepository<Code, Long> {
    Code findByValue(String value);
    Code findByOwner(Account owner);
}