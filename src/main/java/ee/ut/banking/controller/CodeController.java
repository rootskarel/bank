package ee.ut.banking.controller;

import ee.ut.banking.controller.domain.CodeRequest;
import ee.ut.banking.exception.ConstraintException;
import ee.ut.banking.model.Code;
import ee.ut.banking.model.User;
import ee.ut.banking.service.CodeService;
import lombok.RequiredArgsConstructor;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/code", produces = MediaType.APPLICATION_JSON_VALUE)
public class CodeController {

    private final CodeService codeService;

    @GetMapping(value = "/all")
    public List<Code> findAll() {
        return codeService.getAllCodes();
    }

    /*{
        "toAccountName": "EE999999",
        "amount": 10
    }*/
    @PostMapping(value = "/new")
    public ResponseEntity<Code> addNewCode(@RequestBody CodeRequest req) {
        try {
            var result = codeService.createCode(req);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (DataIntegrityViolationException ex) {
            return new ResponseEntity(ex.getMessage(), HttpStatus.CONFLICT);
        } catch (ConstraintException ex) {
            return new ResponseEntity(ex.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    /*{
        .../api/code/by-value?codeValue=SIBUL
    }*/
    @GetMapping(value = {"/by-value"})
    public ResponseEntity<Code> getCodeByCodeValue(@RequestParam String codeValue) {
        try {
            var code = codeService.getCodeByValue(codeValue);
            return new ResponseEntity<>(code, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
}
