package ee.ut.banking.controller;

import ee.ut.banking.controller.domain.CodeTransactionRequest;
import ee.ut.banking.controller.domain.SeedTransactionRequest;
import ee.ut.banking.controller.domain.TransactionRequest;
import ee.ut.banking.controller.domain.TransactionRevokeRequest;
import ee.ut.banking.exception.ConstraintException;
import ee.ut.banking.model.Transaction;
import ee.ut.banking.service.TransactionService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/transaction", produces = MediaType.APPLICATION_JSON_VALUE)
public class TransactionController {

    private final TransactionService transactionService;

    @GetMapping(value = "/all")
    public List<Transaction> findAll() {
        return transactionService.getAllTransactions();
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<Transaction> getTransaction(@PathVariable("id") Long id) {
        var transaction = transactionService.getTransaction(id);

        if (transaction == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(transaction,
                                    HttpStatus.OK);
    }

    /*{
        "amount": 20,
        "fromAccountName": "EE666123456",
        "toAccountName": "EE999999999",
        "fromTransactionId": 1,
        "toTransactionId": 2
    }*/
    @PostMapping(value = "/new")
    public ResponseEntity<Transaction> addNewTransaction(@RequestBody TransactionRequest transaction) {
        try {
            var result = transactionService.makeTransaction(transaction);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (ConstraintException ex) {
            return new ResponseEntity(ex.getMessage(), HttpStatus.CONFLICT);
        }
    }

    /*{
        "id": 1
    }*/
    @PostMapping(value = "/revoke")
    public ResponseEntity<Transaction> revokeTransaction(@RequestBody TransactionRevokeRequest transaction) {
        try {
            var result = transactionService.revokeTransaction(transaction);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (ConstraintException ex) {
            return new ResponseEntity(ex.getMessage(), HttpStatus.CONFLICT);
        }
    }

    /*{
        "fromAccountName": "EE999999",
        "codeValue": "123ABC"
    }*/
    @PostMapping(value = "/new-with-code")
    public ResponseEntity<Integer> addNewTransaction(@RequestBody CodeTransactionRequest codeTransaction) {
        try {
            var result = transactionService.makeCodeTransaction(codeTransaction);
            return new ResponseEntity<>(result,
                                        HttpStatus.OK);
        } catch (ConstraintException ex) {
            return new ResponseEntity(ex.getMessage(),
                                      HttpStatus.CONFLICT);
        }
    }

    /*{
        "id": 1,
        "amount": 20,
        "fromAccountName": "EE666123456",
        "toAccountName": "EE999999999"
    }*/
    @PutMapping(value = "/modify")
    public ResponseEntity<Transaction> modifyTransaction(@RequestBody TransactionRequest req) {
        try {
            var result = transactionService.modifyTransaction(req);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (ConstraintException ex) {
            return new ResponseEntity(ex.getMessage(), HttpStatus.CONFLICT);
        }
    }

    @PostMapping(value = "/seed")
    public ResponseEntity<Transaction> seedTransaction(@RequestBody SeedTransactionRequest req) {
        try {
            var result = transactionService.seedTransaction(req);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (ConstraintException ex) {
            return new ResponseEntity(ex.getMessage(), HttpStatus.CONFLICT);
        }
    }

}
