package ee.ut.banking.controller;

import ee.ut.banking.controller.domain.UserRequest;
import ee.ut.banking.exception.ConstraintException;
import ee.ut.banking.model.User;
import ee.ut.banking.security.domain.UserRole;
import ee.ut.banking.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/user", produces = MediaType.APPLICATION_JSON_VALUE)
public class UserController {

    private final UserService userService;

    @GetMapping(value = "/all")
    public List<User> findAll() {
        return userService.getAllUsers();
    }

    /*{
        "name": "Tõnu Tõukemõnu",
        "username": "tonu@test.ee",
        "accountName": "EE12312312",
    }*/
    @PostMapping(value = "/new")
    public ResponseEntity<User> addNewUser(@RequestBody UserRequest req) {
        try {
            var result = userService.createUser(req);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (DataIntegrityViolationException ex) {
            return new ResponseEntity(ex.getMessage(), HttpStatus.CONFLICT);
        } catch (ConstraintException ex) {
            return new ResponseEntity(ex.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value = {"/info", "/login"})
    public ResponseEntity<User> getUser() {
        try {
            var auth = SecurityContextHolder.getContext().getAuthentication();
            var user = userService.getUserByUsername(auth.getName());

            return new ResponseEntity<>(user, HttpStatus.OK);
        } catch (Exception e) {
            var guestUser = User.builder().role(UserRole.GUEST.toString()).build();

            return new ResponseEntity<>(guestUser, HttpStatus.OK);
        }
    }

    /*{
        .../api/user/by-account?accountName=EE12312312
    }*/
    @GetMapping(value = {"/by-account"})
    public ResponseEntity<User> getUserByAccount(@RequestParam String accountName) {
        try {
            var user = userService.getUserByAccountName(accountName);
            return new ResponseEntity<>(user, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    /*{
         .../api/user/by-code?codeValue=BIEBS
    }*/
    @GetMapping(value = {"/by-code"})
    public ResponseEntity<User> getUserByCode(@RequestParam String codeValue) {
        try {
            var user = userService.getUserByCodeValue(codeValue);
            return new ResponseEntity<>(user, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
}
