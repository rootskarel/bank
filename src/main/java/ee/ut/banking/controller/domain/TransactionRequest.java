package ee.ut.banking.controller.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TransactionRequest {
    @JsonProperty
    long id;
    @JsonProperty
    int amount;
    @JsonProperty
    String fromAccountName;
    @JsonProperty
    String toAccountName;
    @JsonProperty
    Integer fromTransactionId;
    @JsonProperty
    Integer toTransactionId;
}
