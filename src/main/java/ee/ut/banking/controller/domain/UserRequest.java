package ee.ut.banking.controller.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class UserRequest {
    @JsonProperty
    String username;
    @JsonProperty
    String name;
    @JsonProperty
    String accountName;
}
