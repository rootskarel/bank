package ee.ut.banking.controller.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class CodeRequest {
    @JsonProperty
    String toAccountName;
    @JsonProperty
    int amount;
}
