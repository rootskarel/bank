package ee.ut.banking.controller.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class TransactionRevokeRequest {
    @JsonProperty
    long id;
}

