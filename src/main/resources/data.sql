INSERT INTO USERS (id, name, username, password, role)
VALUES (999, 'test', 'test', '$2y$12$Fhv5cIQzD2TstNAZcXsuWuIRJWyRPLcaPM8.xclZyUmosz5gIFPTi', 'USER'),
       (9999, 'test2', 'test2', '$2y$12$Fhv5cIQzD2TstNAZcXsuWuIRJWyRPLcaPM8.xclZyUmosz5gIFPTi', 'ADMIN'),
       (1000, 'test3', 'test3', '$2y$12$Fhv5cIQzD2TstNAZcXsuWuIRJWyRPLcaPM8.xclZyUmosz5gIFPTi', 'USER');

INSERT INTO ACCOUNTS (id, name, balance)
VALUES (1, 'BANK VAULT', 1000000000),
       (2, 'EE666123456', 1000),
       (3, 'EE999999999', 1000),
       (4, 'EE987654321', 1000);

INSERT INTO HAS_ACCOUNT (user_id, account_id)
VALUES (999, 2),
       (9999, 3),
       (1000, 4);