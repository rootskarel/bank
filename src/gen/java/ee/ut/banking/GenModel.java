package ee.ut.banking;

import org.fulib.builder.ClassModelDecorator;
import org.fulib.builder.ClassModelManager;
import org.fulib.builder.Type;
import org.fulib.classmodel.Clazz;

public class GenModel implements ClassModelDecorator {
    public void decorate(ClassModelManager mm) {
        // here you can build your classes like described in
        // https://github.com/fujaba/fulib

        final Clazz user = mm.haveClass("User", c -> {
            c.attribute("id", Type.LONG);
            c.attribute("name", Type.STRING);
            c.attribute("username", Type.STRING);
            c.attribute("password", Type.STRING);
            c.attribute("role", Type.STRING);
        });

        final Clazz account = mm.haveClass("Account", c -> {
            c.attribute("id", Type.LONG);
            c.attribute("balance", Type.INT);
        });

        final Clazz transaction = mm.haveClass("Transaction", c -> {
            c.attribute("id", Type.LONG);
            c.attribute("amount", Type.INT);
            c.attribute("date", "LocalDateTime");
            c.attribute("status", Type.STRING);
        });

        final Clazz code = mm.haveClass("Code", c -> {
            c.attribute("id", Type.LONG);
            c.attribute("amount", Type.INT);
            c.attribute("value", Type.STRING);
        });

        // one user has 0...* accounts, one account can have many users
        mm.associate(user, "accounts", Type.MANY,
                     account, "users", Type.MANY);

        // one account has 0...* sent transactions, one transaction has 1 sender
        mm.associate(account, "sentTransactions", Type.MANY,
                     transaction, "fromAccount", Type.ONE);

        // one account has 0...* received transactions, one transaction has 1 receiver
        mm.associate(account, "receivedTransactions", Type.MANY,
                     transaction, "toAccount", Type.ONE);

        // one from (new) transaction has 0...1 previous (revoked, modified) transactions,
        // one to (revoked, modified) transaction has 1 derived (new) transaction
        mm.associate(transaction, "fromTransaction", Type.ONE,
                     transaction, "toTransaction", Type.ONE);

        // one account has one code, one code has one owner account
        mm.associate(account, "activeCode", Type.ONE,
                     code, "owner", Type.ONE);
    }
}
