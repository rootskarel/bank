import { Account } from "./account";

export class User {
  constructor(
    public id: number,
    public name: string,
    public username: string,
    public password: string,
    public role: UserRole,
    public accounts: Array<Account>
  ) {}
}

export enum UserRole {
  Guest = "GUEST",
  User = "USER",
  Admin = "ADMIN"
}
