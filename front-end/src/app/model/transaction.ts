export class Transaction {
  constructor(
      public id: number,
      public amount: number,
      public date: string,
      public status: TransactionStatus,
      public fromAccount: string,
      public toAccount: string,
      public fromTransaction: number,
      public toTransaction: number
  ) {}
}

export enum TransactionStatus {
  Revoked = "REVOKED",
  Modified = "MODIFIED",
  Active = "ACTIVE"
}
