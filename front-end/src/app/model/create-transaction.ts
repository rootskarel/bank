export class CreateTransaction {
    fromAccountName: string;
    toAccountName: string;
    amount: number;
}