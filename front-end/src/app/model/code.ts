export class Code {
  constructor(
      public id: number,
      public value: string,
      public amount: number,
      public owner: string
  ) {}
}
