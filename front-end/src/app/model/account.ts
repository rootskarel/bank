import { User } from "./user";
import { Transaction } from "./transaction";
import { Code } from "./code";

export class Account {
  constructor(
      public id: number,
      public name: string,
      public balance: number,
      public users: Array<User>,
      public activeCode: Code,
      public sentTransactions: Array<Transaction>,
      public receivedTransactions: Array<Transaction>
  ) {}
}
