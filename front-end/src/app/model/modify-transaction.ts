export class ModifyTransaction {
    id: number;
    amount: number;
    toAccountName: string;
    fromAccountName: string;
}