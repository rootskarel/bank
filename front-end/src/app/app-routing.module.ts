import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from "./component/login/login.component";
import { RegisterComponent } from "./component/register/register.component";
import { P2pTransactionComponent } from "./component/p2p-transaction/p2p-transaction.component";
import { NewTransactionComponent } from "./component/new-transaction/new-transaction.component";
import { NewSeedTransactionComponent } from "./component/new-seed-transaction/new-seed-transaction.component";
import { TransactionHistoryComponent } from "./component/transaction-history/transaction-history.component";
import { TransactionComponent } from "./component/transaction/transaction.component";
import {AuthGuardService} from "./service/auth-guard.service";

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent, canActivate: [AuthGuardService], data: { expectedRole: 'ADMIN'} },
  { path: 'p2p', component: P2pTransactionComponent, canActivate: [AuthGuardService], data: { expectedRole: 'USER'}  },
  { path: 'transactions', component: TransactionHistoryComponent, canActivate: [AuthGuardService], data: { expectedRole: 'ADMIN'}  },
  { path: 'transactions/seed', component: NewSeedTransactionComponent, canActivate: [AuthGuardService], data: { expectedRole: 'ADMIN'}  },
  { path: 'transactions/new', component: NewTransactionComponent, canActivate: [AuthGuardService], data: { expectedRole: 'ADMIN'}  },
  { path: 'transactions/:transactionId', component: TransactionComponent, canActivate: [AuthGuardService], data: { expectedRole: 'ADMIN'}  },
  { path: '',   redirectTo: '/login', pathMatch: 'full' },
  { path: "**", redirectTo: 'login'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
