import { Component, Input } from "@angular/core";
import { faSignOutAlt, faUniversity } from "@fortawesome/free-solid-svg-icons";
import { AuthService } from "../../service/auth.service";
import { UserService } from "../../service/user.service";
import { CodeService } from "../../service/code.service";

@Component({
               selector: "app-header",
               templateUrl: "./header.component.html",
               styleUrls: ["./header.component.scss"]
           })
export class HeaderComponent {
    @Input() title: string;

    faLogout = faSignOutAlt;
    faBank = faUniversity;

    public currentCode: string;
    public currentBalance: number;

    constructor(public authService: AuthService,
                public userService: UserService,
                public codeService: CodeService) {
    }

    onLogout() {
        this.authService.logOut();
    }

    shouldShowCode() {
        return this.authService.isCustomer() && this.currentCode;
    }

    ngOnInit() {
        this.userService.getCurrentBalance()
            .subscribe(balance => {
                this.currentBalance = balance;
            });
        this.codeService.getCurrentCode()
            .subscribe(code => this.currentCode = code);
    }
}
