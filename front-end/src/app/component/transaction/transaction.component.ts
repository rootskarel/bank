import { Component, OnInit, ViewChild } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Subject, Subscription } from "rxjs";
import { TransactionService } from "src/app/service/transaction.service";
import { NgbAlert } from "@ng-bootstrap/ng-bootstrap";
import { catchError, concatMap, debounceTime } from "rxjs/operators";
import { Transaction } from "src/app/model/transaction";
import * as moment from "moment";

@Component({
               selector: "app-login",
               templateUrl: "./transaction.component.html",
               styleUrls: ["./transaction.component.scss"],
               providers: [TransactionService]
           })
export class TransactionComponent implements OnInit {
    private subscription: Subscription = new Subscription();

    isEditMode = false;
    isActive = true;

    transaction: Transaction;

    transactionForm = new FormGroup({
                                        fromAccountName: new FormControl("", [Validators.required]),
                                        toAccountName: new FormControl("", [Validators.required]),
                                        amount: new FormControl("", [Validators.required]),
                                    });

    ngOnDestroy(): void {
        this.subscription.unsubscribe();
    }

    @ViewChild("ngbAlert", {static: false}) ngbAlert: NgbAlert;
    private _alertSubject = new Subject<any>();
    alert: any;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private transactionService: TransactionService) {
    }

    ngOnInit(): void {
        const alertSub = this._alertSubject.subscribe(alert => this.alert = alert);
        this._alertSubject.pipe(debounceTime(5000))
            .subscribe(() => {
                if (this.ngbAlert) {
                    this.ngbAlert.close();
                }
            });

        const transactionSub = this.route.params.pipe(
            concatMap(params => this.transactionService.getTransaction(params.transactionId)))
                                   .pipe(catchError(_ => this.router.navigateByUrl("/transactions")))
                                   .subscribe((transaction: Transaction) => {
                                       this.changeEditMode(false);
                                       this.updateTransaction(transaction);
                                       this.transactionForm.controls.fromAccountName.setValue(transaction.fromAccount);
                                       this.transactionForm.controls.toAccountName.setValue(transaction.toAccount);
                                       this.transactionForm.controls.amount.setValue(transaction.amount);
                                       this.isActive = this.transaction.status === "ACTIVE";
                                   });

        this.subscription.add(transactionSub);
        this.subscription.add(alertSub);
    }

    async goToTransaction(id: number) {
        await this.router.navigateByUrl(`transactions/${id}`);
    }

    changeEditMode(turnTo: boolean): void {
        this.isEditMode = turnTo;
    }

    updateTransaction(transaction: Transaction) {
        transaction.date = moment(transaction.date).format("dddd, MMMM Do YYYY, HH:mm");
        this.transaction = transaction;
    }

    modifyTransaction(): void {
        this.transactionForm.value.id = this.transaction.id;
        const requestSub = this.transactionService
                               .modifyTransaction(this.transactionForm.value)
                               .subscribe((newTransaction: Transaction) => {
                                   this._alertSubject.next(
                                       {message: "Transaction modified successfully!", type: "success"});
                                   this.changeEditMode(false);
                                   this.goToTransaction(newTransaction.id);
                               }, (error) => {
                                   this._alertSubject.next({message: error.error, type: "danger"});
                               });
        this.subscription.add(requestSub);
    }

    resetError() {
        this.alert = null;
    }

    revoke(): void {
        this.transactionForm.value.id = this.transaction.id;
        const requestSub = this.transactionService.revokeTransaction(this.transactionForm.value.id)
                               .subscribe((newTransaction: Transaction) => {
                                   this._alertSubject.next(
                                       {message: "Transaction revoked successfully!", type: "success"});
                                   this.goToTransaction(newTransaction.id);
                               }, (error) => {
                                   this._alertSubject.next({message: error.error, type: "danger"});
                               });
        this.subscription.add(requestSub);
    }
}
