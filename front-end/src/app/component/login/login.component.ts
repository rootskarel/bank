import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { faLock, faUser } from "@fortawesome/free-solid-svg-icons";
import { AuthService } from "../../service/auth.service";
import { Router } from "@angular/router";

@Component({
               selector: "app-login",
               templateUrl: "./login.component.html",
               styleUrls: ["./login.component.scss"]
           })
export class LoginComponent implements OnInit {
    faUser = faUser;
    faLock = faLock;
    username = "";
    password = "";
    shouldShowError = false;

    loginForm = new FormGroup({
                                  name: new FormControl("", [Validators.required]),
                                  password: new FormControl("", [Validators.required]),
                              });

    constructor(private authService: AuthService, private router: Router) {
    }

    async ngOnInit() {
        this.authService.loadUserInfo().subscribe(async () => {
            await this.authService.navigateIfAuthenticated();
        });
    }

    onSubmit() {
        this.shouldShowError = false;
        this.authService.authenticate(this.username, this.password)
            .subscribe(() =>
                           this.authService.navigateIfAuthenticated(),
                       _ => this.shouldShowError = true
            );
    }
}
