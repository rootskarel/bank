import { Component, OnInit, ViewChild } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from '@angular/router';
import { NgbAlert, NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { Subject } from "rxjs";
import { TransactionService } from "src/app/service/transaction.service";
import { concatMap, debounceTime } from 'rxjs/operators';
import { ConfirmModalComponent } from "../transaction-confirm-modal/confirm-modal.component";

@Component({
             selector: "app-new-transaction",
             templateUrl: "./new-transaction.component.html",
             styleUrls: ["./new-transaction.component.scss"]
           })
export class NewTransactionComponent implements OnInit {
  transactionForm = new FormGroup({
                                    fromAccountName: new FormControl("", [Validators.required]),
    toAccountName: new FormControl('', [Validators.required]),
    amount: new FormControl('0', [Validators.required]),
  });

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private modalService: NgbModal,
    private transactionService: TransactionService) { }

  @ViewChild('errorAlert', { static: false }) errorAlert: NgbAlert;
  private _error = new Subject<string>();
  errorMessage: string;

  ngOnInit(): void {
    this._error.subscribe(message => this.errorMessage = message);
    this._error.pipe(debounceTime(5000)).subscribe(() => {
      if (this.errorAlert) {
        this.errorAlert.close();
      }
    });
  }

  onSubmit(): void {
    this.transactionService.createTransaction(this.transactionForm.value).pipe(
      concatMap(() => {
        const modalRef = this.modalService.open(ConfirmModalComponent);
        modalRef.componentInstance.success = true;
        return modalRef.result;
      }))
      .subscribe(() => {
        this.router.navigate(['../'], { relativeTo: this.route })
      }, (error) => {
        this._error.next(error.error);
      })
  }
}
