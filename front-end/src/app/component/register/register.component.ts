import { Component, OnInit, ViewChild } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { NgbAlert, NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { Subject, Subscription } from "rxjs";
import { debounceTime } from "rxjs/operators";
import { RegisterConfirmModalComponent } from "./register-confirm-modal/register-confirm-modal.component";
import { UserService } from "src/app/service/user.service";
import { User } from "../../model/user";

@Component({
               selector: "app-register",
               templateUrl: "./register.component.html",
               styleUrls: ["./register.component.scss"],
               providers: [UserService]
           })
export class RegisterComponent implements OnInit {
    registerForm = new FormGroup({
                                     username: new FormControl("", [Validators.required]),
                                     name: new FormControl("", [Validators.required]),
                                     accountName: new FormControl("", [Validators.required]),
                                 });

    private subscription: Subscription = new Subscription();

    @ViewChild("ngbAlert", {static: false}) ngbAlert: NgbAlert;
    private _alertSubject = new Subject<any>();
    alert: any;

    constructor(
        private modalService: NgbModal,
        private userService: UserService) {
    }

    ngOnDestroy(): void {
        this.subscription.unsubscribe();
    }

    ngOnInit(): void {
        const alertSub = this._alertSubject.subscribe(alert => this.alert = alert);
        this._alertSubject.pipe(debounceTime(5000))
            .subscribe(() => {
                if (this.ngbAlert) {
                    this.ngbAlert.close();
                }
            });
        this.subscription.add(alertSub);
    }

    resetError() {
        this.alert = null;
    }

    onSubmit(): void {

        this.userService.createUser(this.registerForm.value)
            .subscribe((user: User) => {
                           const modalRef = this.modalService.open(RegisterConfirmModalComponent);
                           modalRef.componentInstance.username = this.registerForm.value.username;
                           modalRef.componentInstance.password = user.password;
                       },
                       error => {
                           this._alertSubject.next({message: error.error, type: "danger"});
                       });

    }

}
