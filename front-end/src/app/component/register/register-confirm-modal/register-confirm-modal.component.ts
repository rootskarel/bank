import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-register-confirm-modal',
  templateUrl: './register-confirm-modal.component.html',
  styleUrls: ['./register-confirm-modal.component.scss']
})
export class RegisterConfirmModalComponent implements OnInit {
  @Input() username: string;
  @Input() password: string;

  constructor(public activeModal: NgbActiveModal) {}

  ngOnInit(): void {
  }

}
