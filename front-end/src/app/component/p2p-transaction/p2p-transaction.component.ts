import { Component, OnInit, ViewChild } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { NgbAlert, NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { ConfirmModalComponent } from "../transaction-confirm-modal/confirm-modal.component";
import { PaymentModalComponent } from "./payment-modal/payment-modal.component";
import { RequestModalComponent } from "./request-modal/request-modal.component";
import { CodeService } from "../../service/code.service";
import { debounceTime } from "rxjs/operators";
import { TransactionService } from "../../service/transaction.service";
import { UserService } from "../../service/user.service";
import { Subject } from "rxjs";
import { Code } from "../../model/code";

@Component({
               selector: "app-p2p-transaction",
               templateUrl: "./p2p-transaction.component.html",
               styleUrls: ["./p2p-transaction.component.scss"]
           })
export class P2pTransactionComponent implements OnInit {

    @ViewChild("errorAlertRequest", {static: false}) errorAlertRequest: NgbAlert;
    private _errorRequest = new Subject<string>();
    @ViewChild("errorAlertPayment", {static: false}) errorAlertPayment: NgbAlert;
    private _errorPayment = new Subject<string>();
    errorMsgRequest: string = "";
    errorMsgPayment: string = "";

    requestForm = new FormGroup({
                                    amount: new FormControl("", [Validators.required])
                                });

    paymentForm = new FormGroup({
                                    code: new FormControl("", [Validators.required])
                                });

    constructor(private modalService: NgbModal,
                private userService: UserService,
                private codeService: CodeService,
                private transactionService: TransactionService) {
    }

    ngOnInit(): void {
        this._errorRequest.subscribe(message => this.errorMsgRequest = message);
        this._errorRequest.pipe(debounceTime(5000)).subscribe(() => {
            if (this.errorAlertRequest) {
                this.errorAlertRequest.close();
            }
        });
        this._errorPayment.subscribe(message => this.errorMsgPayment = message);
        this._errorPayment.pipe(debounceTime(5000)).subscribe(() => {
            if (this.errorAlertPayment) {
                this.errorAlertPayment.close();
            }
        });
    }

    openPaymentModal() {
        const code = this.paymentForm.value.code;
        this.codeService.getCodeByValue(code).subscribe(code => {
            this.makePayment(code.amount, code.owner);
        }, error => {
            if (this.modalService.hasOpenModals()) {
                this.modalService.dismissAll();
            }
            this._errorPayment.next(`${error.error}`);
        });
    }

    openRequestModal() {
        return this.codeService.createCode(this.userService.getCurrentUserAccount().getValue().name,
                                           this.requestForm.value.amount)
                   .subscribe((code: Code) => {
                       this.codeService.setCurrentCode(code.value);
                       this.generateRequestCode(code.value);
                   }, (err) => this._errorRequest.next(`${err.error}`));

    }

    generateRequestCode(codeValue: String) {
        const modalRef = this.modalService.open(RequestModalComponent);
        modalRef.componentInstance.code = codeValue;
        modalRef.componentInstance.amount = this.requestForm.value.amount;
    }

    makePayment(amount: number, receiver: string) {
        const modalRef = this.modalService.open(PaymentModalComponent);
        modalRef.componentInstance.receiver = receiver;
        modalRef.componentInstance.amount = amount;

        modalRef.result.then((result) => {
            if (result == "confirm") {
                return this.transactionService.createTransactionFromCode(
                    this.userService.getCurrentUser().getValue()?.accounts[0]?.name,
                    this.paymentForm.value.code)
                           .subscribe((newBalance: number) => {
                               this.userService.setCurrentBalance(newBalance);
                               const modalRef = this.modalService.open(ConfirmModalComponent);
                               modalRef.componentInstance.success = true;
                           }, err => {
                               this.modalService.dismissAll();
                               this._errorPayment.next(err.error);
                           });
            }
        });
    }
}
