import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-request-modal',
  templateUrl: './request-modal.component.html',
  styleUrls: ['./request-modal.component.scss']
})
export class RequestModalComponent implements OnInit {
  @Input() amount: number;
  @Input() code: String;
  constructor(public activeModal: NgbActiveModal) {}

  ngOnInit(): void {
  }

}
