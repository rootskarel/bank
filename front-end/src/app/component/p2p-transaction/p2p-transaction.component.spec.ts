import { ComponentFixture, TestBed } from '@angular/core/testing';

import { P2pTransactionComponent } from './p2p-transaction.component';

describe('P2pTransactionComponent', () => {
  let component: P2pTransactionComponent;
  let fixture: ComponentFixture<P2pTransactionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ P2pTransactionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(P2pTransactionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
