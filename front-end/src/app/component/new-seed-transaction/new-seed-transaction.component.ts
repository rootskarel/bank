import { Component, OnInit, ViewChild } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { NgbAlert, NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { Subject } from "rxjs";
import { concatMap, debounceTime } from "rxjs/operators";
import { TransactionService } from "src/app/service/transaction.service";
import { ConfirmModalComponent } from "../transaction-confirm-modal/confirm-modal.component";

@Component({
             selector: "app-new-seed-transaction",
             templateUrl: "./new-seed-transaction.component.html",
             styleUrls: ["./new-seed-transaction.component.scss"]
           })
export class NewSeedTransactionComponent implements OnInit {
  seedForm = new FormGroup({
                             toAccountName: new FormControl("", [Validators.required]),
    amount: new FormControl('0', [Validators.required]),
  });

  @ViewChild('errorAlert', { static: false }) errorAlert: NgbAlert;
  private _error = new Subject<string>();
  errorMessage: string;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private modalService: NgbModal,
    private transactionService: TransactionService) { }

  ngOnInit(): void {
    this._error.subscribe(message => this.errorMessage = message);
    this._error.pipe(debounceTime(5000)).subscribe(() => {
      if (this.errorAlert) {
        this.errorAlert.close();
      }
    });
  }

  onSubmit(): void {
    this.transactionService.seed(this.seedForm.value).pipe(
      concatMap(() => {
        const modalRef = this.modalService.open(ConfirmModalComponent);
        modalRef.componentInstance.success = true;
        return modalRef.result;
      })
    ).subscribe(() => {
      this.router.navigate(['../'], { relativeTo: this.route });
    }, (error) => {
      this._error.next(error.error);
    })
  }

}
