import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewSeedTransactionComponent } from './new-seed-transaction.component';

describe('NewSeedTransactionComponent', () => {
  let component: NewSeedTransactionComponent;
  let fixture: ComponentFixture<NewSeedTransactionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewSeedTransactionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewSeedTransactionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
