import { Component, OnInit } from "@angular/core";
import { TransactionService } from "../../service/transaction.service";
import { Transaction } from "../../model/transaction";
import * as moment from "moment";

@Component({
               selector: "app-login",
               templateUrl: "./transaction-history.component.html",
               styleUrls: ["./transaction-history.component.scss"]
           })
export class TransactionHistoryComponent implements OnInit {
    transactions = [];
    headElements = ["ID", "Status", "Amount", "From account", "To account", "Date created", "Action"];

    constructor(
        private transactionService: TransactionService) {
    }

  ngOnInit(): void {
      this.transactionService.getAllTransactions()
          .subscribe((transactions: Transaction[]) => {
              this.transactions = transactions.map(transaction => {
                  transaction.date = moment(transaction.date).format("dddd, MMMM Do YYYY, HH:mm");
                  return transaction;
              });
          });
  }
}
