import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { HTTP_INTERCEPTORS, HttpClientModule } from "@angular/common/http";
import { AuthInterceptor } from "./interceptor/auth.interceptor";
import { AuthService } from "./service/auth.service";
import { HttpService } from "./service/http.service";
import { CookieService } from "ngx-cookie-service";
import { HeaderComponent } from "./component/header/header.component";
import { LoginComponent } from "./component/login/login.component";
import { TransactionComponent } from "./component/transaction/transaction.component";
import { TransactionHistoryComponent } from "./component/transaction-history/transaction-history.component";
import { NewTransactionComponent } from "./component/new-transaction/new-transaction.component";
import { NewSeedTransactionComponent } from "./component/new-seed-transaction/new-seed-transaction.component";
import { RegisterComponent } from "./component/register/register.component";
import { P2pTransactionComponent } from "./component/p2p-transaction/p2p-transaction.component";
import { PaymentModalComponent } from "./component/p2p-transaction/payment-modal/payment-modal.component";
import { RequestModalComponent } from "./component/p2p-transaction/request-modal/request-modal.component";
import { ConfirmModalComponent } from "./component/transaction-confirm-modal/confirm-modal.component";
import { UserService } from "./service/user.service";
import { CodeService } from "./service/code.service";
import { TransactionService } from "./service/transaction.service";

@NgModule({
              declarations: [
                  AppComponent,
                  LoginComponent,
                  TransactionComponent,
                  TransactionHistoryComponent,
                  NewTransactionComponent,
                  NewSeedTransactionComponent,
                  RegisterComponent,
                  P2pTransactionComponent,
                  PaymentModalComponent,
                  RequestModalComponent,
                  ConfirmModalComponent,
                  HeaderComponent,
              ],
              imports: [
                  BrowserModule,
                  AppRoutingModule,
                  NgbModule,
                  FormsModule,
                  ReactiveFormsModule,
                  FontAwesomeModule,
                  HttpClientModule,
              ],
              providers: [
                  AuthService,
                  HttpService,
                  CookieService,
                  UserService,
                  CodeService,
                  TransactionService,
                  {
                      provide: HTTP_INTERCEPTORS,
                      useClass: AuthInterceptor,
                      multi: true,
                  }
              ],
              bootstrap: [AppComponent]
          })
export class AppModule {
}
