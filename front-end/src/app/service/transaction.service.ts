import { Injectable } from "@angular/core";
import { CreateTransaction } from "../model/create-transaction";
import { SeedTransaction } from "../model/seed-transaction";
import { ModifyTransaction } from "../model/modify-transaction";
import { HttpService } from "./http.service";
import { catchError, map } from "rxjs/operators";
import { Transaction } from "../model/transaction";

@Injectable()
export class TransactionService {

  constructor(private httpService: HttpService) {
  }

  public createTransaction(transaction: CreateTransaction) {
    return this.httpService.post("/transaction/new/", transaction);
  }

  public getTransaction(id: number) {
    return this.httpService.get(`/transaction/${id}`);
  }

  public revokeTransaction(id: number)  {
    return this.httpService.post('/transaction/revoke/', {id: id});
  }

  public modifyTransaction(transaction: ModifyTransaction)  {
    return this.httpService.put('/transaction/modify/', transaction);
  }

  public createTransactionFromCode(fromAccountName: string, code: string)  {
    return this.httpService.post('/transaction/new-with-code/', {fromAccountName: fromAccountName, codeValue: code});
  }

  public seed(seedTransaction: SeedTransaction)  {
    return this.httpService.post('/transaction/seed/', seedTransaction);
  }

  public getAllTransactions() {
    return this.httpService.get("/transaction/all")
               .pipe(map((transactions: Transaction[]) => transactions,
                         catchError(this.httpService.handleError)));
  }
}
