import { Injectable } from "@angular/core";
import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams } from "@angular/common/http";
import { Observable, throwError } from "rxjs";

export interface User {
    name: string;
    username: string;

}

@Injectable()
export class HttpService {
    apiBaseUrl = "/api";

    constructor(private http: HttpClient) {
    }

    handleError(err: HttpErrorResponse) {

        let errorMessage = "";
        if (err.error instanceof ErrorEvent) {
            errorMessage = `An error occurred: ${err.error.message}`;
        } else {

            errorMessage = `Server returned code: ${err.status}, error message is: ${err.message}`;
        }
        console.error(errorMessage);
        return throwError(errorMessage);
    }

    get(path: string, params?: HttpParams, headers?: HttpHeaders): Observable<any> {
        return this.http.get(this.apiBaseUrl + path, {params, headers});
    }

    post(path: string, body?: object, params?: HttpParams): Observable<any> {
        return this.http.post(this.apiBaseUrl + path, body, {params});
    }

    put(path: string, body?: object, params?: HttpParams): Observable<any> {
        return this.http.put(this.apiBaseUrl + path, body, {params});
    }
}
