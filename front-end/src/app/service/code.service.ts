import { Injectable } from "@angular/core";
import { HttpService } from "./http.service";
import { HttpParams } from "@angular/common/http";
import { map } from "rxjs/operators";
import { Code } from "../model/code";
import { BehaviorSubject } from "rxjs";

@Injectable()
export class CodeService {

    private readonly currentCode: BehaviorSubject<string>;

    constructor(private httpService: HttpService) {
        this.currentCode = new BehaviorSubject<string>(undefined);
    }

    public createCode(accountName: string, amount: number) {
        return this.httpService.post("/code/new/", {toAccountName: accountName, amount: amount})
                   .pipe(map((code: Code) => code));
    }

    public getCodeByValue(codeValue: string) {
        const params = new HttpParams().set("codeValue", codeValue);
        return this.httpService.get("/code/by-value", params).pipe(
            map((code: Code) => code));
    }

    getCurrentCode() {
        return this.currentCode;
    }

    setCurrentCode(code: string) {
        this.currentCode.next(code);
    }
}
