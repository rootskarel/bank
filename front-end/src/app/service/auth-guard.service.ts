import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, CanActivate, Router } from "@angular/router";
import { AuthService } from "./auth.service";
import { Observable } from "rxjs";
import { map, tap } from "rxjs/operators";

@Injectable({
                providedIn: "root"
            })
export class AuthGuardService implements CanActivate {

    constructor(public authService: AuthService, public router: Router) {
    }

    canActivate(route: ActivatedRouteSnapshot): Observable<boolean> {
        const expectedRole = route.data.expectedRole;
        return this.authService.loadUserInfo().pipe(
            map(user => user.role === expectedRole),
            tap(isAllowed => {
                if (!isAllowed) {
                    this.router.navigate(["login"]);
                }
            }));
    }
}

