import { Injectable } from "@angular/core";
import { HttpService } from "./http.service";
import { CookieService } from "ngx-cookie-service";
import { Router } from "@angular/router";
import { catchError, map, tap } from "rxjs/operators";
import { User, UserRole } from "../model/user";
import { UserService } from "./user.service";
import { BehaviorSubject } from "rxjs";
import { CodeService } from "./code.service";

@Injectable()
export class AuthService {
    loginReqUrl = "/user/login";
    userInfoReqUrl = "/user/info";
    authToken = "";
    authCookieKey = "user";

    private readonly userRole: BehaviorSubject<UserRole>;

    constructor(private httpService: HttpService,
                private cookieService: CookieService,
                private router: Router,
                private userService: UserService,
                private codeService: CodeService) {
        this.userRole = new BehaviorSubject<UserRole>(UserRole.Guest);
    }

    isAuthenticated() {
        return this.getUserRole().getValue() !== UserRole.Guest;
    }

    isAdmin() {
        return this.getUserRole().getValue() === UserRole.Admin;
    }

    isCustomer() {
        return this.getUserRole().getValue() === UserRole.User;
    }

    getUserRole() {
        return this.userRole;
    }

    getAuthToken() {
        return this.authToken;
    }

    setAuthTokenFromCredentials(username: string, password: string) {
        this.setAuthToken(btoa(`${username}:${password}`));
    }

    setAuthToken(token: string) {
        this.authToken = token;
        this.cookieService.set(this.authCookieKey, token);
    }

    authenticate(username: string, password: string) {
        this.setAuthTokenFromCredentials(username, password);
        return this.httpService.get(this.loginReqUrl).pipe(tap((user: User) => {
            this.updateUserDetails(user);
        }));
    }

    updateUserDetails(user: User) {
        this.userService.setCurrentUser(user);
        this.userService.setCurrentBalance(user?.accounts[0]?.balance);
        this.userService.setCurrentUserAccount(user?.accounts[0]);
        this.setUserRole(user.role);
        this.codeService.setCurrentCode(user?.accounts[0]?.activeCode?.value);
    }

    setUserRole(role: UserRole) {
        this.userRole.next(role);
    }

    loadUserInfo() {
        return this.httpService.get(this.userInfoReqUrl).pipe(
            map((user: User) => {
                this.updateUserDetails(user);
                return user;
            }),
            catchError(this.httpService.handleError)
        );
    }

    logOut() {
        this.httpService.post("/logout").subscribe((async () => {
            this.authToken = "";
            this.userService.clearUserInfo();
            this.setUserRole(UserRole.Guest);
            this.cookieService.delete("user");
            await this.router.navigateByUrl("/");
        }));
    }

    async navigateIfAuthenticated() {
        if (this.isCustomer()) {
            await this.router.navigateByUrl("/p2p");
        } else if (this.isAdmin()) {
            await this.router.navigateByUrl("/transactions");
        } else {
            await this.router.navigateByUrl("/");
        }
    }
}
