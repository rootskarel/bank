import { Injectable } from "@angular/core";
import { HttpService } from "./http.service";
import { HttpParams } from "@angular/common/http";
import { User } from "../model/user";
import { BehaviorSubject, } from "rxjs";
import { UserRegistration } from "../model/user-registration";
import { map } from "rxjs/operators";
import { Account } from "../model/account";

@Injectable()
export class UserService {

    private readonly currentUser: BehaviorSubject<User>;
    private readonly currentBalance: BehaviorSubject<number>;
    private readonly currentUserAccount: BehaviorSubject<Account>;

    constructor(private httpService: HttpService) {
        this.currentUser = new BehaviorSubject<User>(undefined);
        this.currentBalance = new BehaviorSubject<number>(undefined);
        this.currentUserAccount = new BehaviorSubject<Account>(undefined);
    }

    public clearUserInfo() {
        this.setCurrentBalance(undefined);
        this.setCurrentUser(undefined);
        this.setCurrentUserAccount(undefined);
    }

    public createUser(userRegistration: UserRegistration) {
        return this.httpService.post("/user/new/", userRegistration);
    }

    public userByCode(codeValue: string) {
        const params = new HttpParams().set("codeValue", codeValue);
        return this.httpService.get("/user/by-code", params)
                   .pipe(map((user: User) => user));
    }

    getCurrentUser() {
        return this.currentUser;
    }

    getCurrentBalance() {
        return this.currentBalance;
    }

    setCurrentUser(user: User) {
        this.currentUser.next(user);
    }

    getCurrentUserAccount() {
        return this.currentUserAccount;
    }

    setCurrentUserAccount(account: Account) {
        this.currentUserAccount.next(account);
    }

    setCurrentBalance(balance: number) {
        this.currentBalance.next(balance);
    }
}
