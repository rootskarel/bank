import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { AuthService } from "../service/auth.service";

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

    constructor(private authService: AuthService) {
    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const token = this.authService.getAuthToken();
        req = req.clone({
                            setHeaders: {
                                'X-Requested-With': 'XMLHttpRequest',
                                'Authorization': `Basic ${token}`,
                                'Accept': 'application/json'
                            },
                        });

        return next.handle(req);
    }
}