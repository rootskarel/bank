import { Component, OnInit } from "@angular/core";
import { AuthService } from "./service/auth.service";
import { CookieService } from "ngx-cookie-service";

@Component({
               selector: "app-root",
               templateUrl: "./app.component.html",
               styleUrls: ["./app.component.scss"]
           })
export class AppComponent implements OnInit {
    title = "Awesome Bank Ltd";
    showButtons = false;

    constructor(private authService: AuthService,
                private cookieService: CookieService) {
    }

    async ngOnInit() {
        const cookieToken = this.cookieService.get("user");
        if (cookieToken) {
            this.authService.setAuthToken(cookieToken);
        }
    }
}
